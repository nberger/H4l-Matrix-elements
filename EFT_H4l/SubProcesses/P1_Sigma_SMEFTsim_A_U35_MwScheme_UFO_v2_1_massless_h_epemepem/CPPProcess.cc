//==========================================================================
// This file has been automatically generated for C++ Standalone by
// MadGraph5_aMC@NLO v. 2.6.5, 2018-02-03
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "CPPProcess.h"
#include "HelAmps_SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless.h"

using namespace MG5_SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless; 
namespace MG5_Sigma_SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless_h_epemepem {

//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: h > e+ e- e+ e- NP<=1 QED<=3 @1
// Process: h > mu+ mu- mu+ mu- NP<=1 QED<=3 @1

//--------------------------------------------------------------------------
// Initialize process.

void CPPProcess::initProc(string param_card_name) 
{
  // Instantiate the model class and set parameters that stay fixed during run
  pars = new Parameters_SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless(); 
  SLHAReader slha(param_card_name); 
  pars->setIndependentParameters(slha); 
  pars->setIndependentCouplings(); 
  pars->printIndependentParameters(); 
  pars->printIndependentCouplings(); 
  // Set external particle masses for this matrix element
  mME.push_back(pars->mdl_MH); 
  mME.push_back(pars->mdl_Me); 
  mME.push_back(pars->mdl_Me); 
  mME.push_back(pars->mdl_Me); 
  mME.push_back(pars->mdl_Me); 
  jamp2[0] = new double[1]; 
}

//--------------------------------------------------------------------------
// Evaluate |M|^2, part independent of incoming flavour.

void CPPProcess::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  static bool firsttime = true; 
  if (firsttime)
  {
    pars->printDependentParameters(); 
    pars->printDependentCouplings(); 
    firsttime = false; 
  }

  // Reset color flows
  for(int i = 0; i < 1; i++ )
    jamp2[0][i] = 0.; 

  // Local variables and constants
  const int ncomb = 16; 
  static bool goodhel[ncomb] = {ncomb * false}; 
  static int ntry = 0, sum_hel = 0, ngood = 0; 
  static int igood[ncomb]; 
  static int jhel; 
  std::complex<double> * * wfs; 
  double t[nprocesses]; 
  // Helicities for the process
  static const int helicities[ncomb][nexternal] = {{0, -1, -1, -1, -1}, {0, -1,
      -1, -1, 1}, {0, -1, -1, 1, -1}, {0, -1, -1, 1, 1}, {0, -1, 1, -1, -1},
      {0, -1, 1, -1, 1}, {0, -1, 1, 1, -1}, {0, -1, 1, 1, 1}, {0, 1, -1, -1,
      -1}, {0, 1, -1, -1, 1}, {0, 1, -1, 1, -1}, {0, 1, -1, 1, 1}, {0, 1, 1,
      -1, -1}, {0, 1, 1, -1, 1}, {0, 1, 1, 1, -1}, {0, 1, 1, 1, 1}};
  // Denominators: spins, colors and identical particles
  const int denominators[nprocesses] = {4}; 

  ntry = ntry + 1; 

  // Reset the matrix elements
  for(int i = 0; i < nprocesses; i++ )
  {
    matrix_element[i] = 0.; 
  }
  // Define permutation
  int perm[nexternal]; 
  for(int i = 0; i < nexternal; i++ )
  {
    perm[i] = i; 
  }

  if (sum_hel == 0 || ntry < 10)
  {
    // Calculate the matrix element for all helicities
    for(int ihel = 0; ihel < ncomb; ihel++ )
    {
      if (goodhel[ihel] || ntry < 2)
      {
        calculate_wavefunctions(perm, helicities[ihel]); 
        t[0] = matrix_1_h_epemepem(); 

        double tsum = 0; 
        for(int iproc = 0; iproc < nprocesses; iproc++ )
        {
          matrix_element[iproc] += t[iproc]; 
          tsum += t[iproc]; 
        }
        // Store which helicities give non-zero result
        if (tsum != 0. && !goodhel[ihel])
        {
          goodhel[ihel] = true; 
          ngood++; 
          igood[ngood] = ihel; 
        }
      }
    }
    jhel = 0; 
    sum_hel = min(sum_hel, ngood); 
  }
  else
  {
    // Only use the "good" helicities
    for(int j = 0; j < sum_hel; j++ )
    {
      jhel++; 
      if (jhel >= ngood)
        jhel = 0; 
      double hwgt = double(ngood)/double(sum_hel); 
      int ihel = igood[jhel]; 
      calculate_wavefunctions(perm, helicities[ihel]); 
      t[0] = matrix_1_h_epemepem(); 

      for(int iproc = 0; iproc < nprocesses; iproc++ )
      {
        matrix_element[iproc] += t[iproc] * hwgt; 
      }
    }
  }

  for (int i = 0; i < nprocesses; i++ )
    matrix_element[i] /= denominators[i]; 



}

//--------------------------------------------------------------------------
// Evaluate |M|^2, including incoming flavour dependence.

double CPPProcess::sigmaHat() 
{
  // Select between the different processes
  if(id1 == 25 && id2 == -11)
  {
    // Add matrix elements for processes with beams (25, -11)
    return matrix_element[0]; 
  }
  else if(id1 == 25 && id2 == -13)
  {
    // Add matrix elements for processes with beams (25, -13)
    return matrix_element[0]; 
  }
  else
  {
    // Return 0 if not correct initial state assignment
    return 0.; 
  }
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void CPPProcess::calculate_wavefunctions(const int perm[], const int hel[])
{
  // Calculate wavefunctions for all processes
  int i, j; 

  // Calculate all wavefunctions
  sxxxxx(p[perm[0]], -1, w[0]); 
  ixxxxx(p[perm[1]], mME[1], hel[1], -1, w[1]); 
  oxxxxx(p[perm[2]], mME[2], hel[2], +1, w[2]); 
  ixxxxx(p[perm[3]], mME[3], hel[3], -1, w[3]); 
  oxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  FFV1_6_3(w[1], w[2], -pars->GC_234, pars->GC_249, pars->mdl_MZ, pars->mdl_WZ,
      w[5]);
  FFV5P0_3(w[1], w[2], -pars->GC_3, pars->ZERO, pars->ZERO, w[6]); 
  FFV5P0_3(w[3], w[4], -pars->GC_3, pars->ZERO, pars->ZERO, w[7]); 
  FFV1_6_3(w[3], w[4], -pars->GC_234, pars->GC_249, pars->mdl_MZ, pars->mdl_WZ,
      w[8]);
  FFV3_5_3(w[3], w[4], pars->GC_337, pars->GC_328, pars->mdl_MZ, pars->mdl_WZ,
      w[9]);
  FFV1_6_3(w[3], w[4], pars->GC_344, pars->GC_349, pars->mdl_MZ, pars->mdl_WZ,
      w[10]);
  FFV3_5_3(w[1], w[2], pars->GC_337, pars->GC_328, pars->mdl_MZ, pars->mdl_WZ,
      w[11]);
  FFV1_6_3(w[1], w[2], pars->GC_344, pars->GC_349, pars->mdl_MZ, pars->mdl_WZ,
      w[12]);
  FFV1_6_3(w[1], w[4], -pars->GC_234, pars->GC_249, pars->mdl_MZ, pars->mdl_WZ,
      w[13]);
  FFV5P0_3(w[1], w[4], -pars->GC_3, pars->ZERO, pars->ZERO, w[14]); 
  FFV5P0_3(w[3], w[2], -pars->GC_3, pars->ZERO, pars->ZERO, w[15]); 
  FFV1_6_3(w[3], w[2], -pars->GC_234, pars->GC_249, pars->mdl_MZ, pars->mdl_WZ,
      w[16]);
  FFV3_5_3(w[3], w[2], pars->GC_337, pars->GC_328, pars->mdl_MZ, pars->mdl_WZ,
      w[17]);
  FFV1_6_3(w[3], w[2], pars->GC_344, pars->GC_349, pars->mdl_MZ, pars->mdl_WZ,
      w[18]);
  FFV3_5_3(w[1], w[4], pars->GC_337, pars->GC_328, pars->mdl_MZ, pars->mdl_WZ,
      w[19]);
  FFV1_6_3(w[1], w[4], pars->GC_344, pars->GC_349, pars->mdl_MZ, pars->mdl_WZ,
      w[20]);

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  FFVS1_3_0(w[3], w[4], w[5], w[0], pars->GC_315, pars->GC_313, amp[0]); 
  VVS2_0(w[6], w[7], w[0], pars->GC_325, amp[1]); 
  VVS4_0(w[6], w[7], w[0], pars->GC_322, amp[2]); 
  VVS1_0(w[6], w[8], w[0], pars->GC_324, amp[3]); 
  VVS4_0(w[6], w[8], w[0], pars->GC_324, amp[4]); 
  VVS1_0(w[7], w[5], w[0], pars->GC_324, amp[5]); 
  VVS4_0(w[7], w[5], w[0], pars->GC_324, amp[6]); 
  VVS3_0(w[5], w[8], w[0], pars->GC_319, amp[7]); 
  VVS2_4_0(w[5], w[8], w[0], pars->GC_321, pars->GC_320, amp[8]); 
  VVS3_0(w[5], w[8], w[0], pars->GC_358, amp[9]); 
  VVS3_0(w[5], w[9], w[0], pars->GC_319, amp[10]); 
  VVS3_0(w[5], w[10], w[0], pars->GC_319, amp[11]); 
  VVS3_0(w[11], w[8], w[0], pars->GC_319, amp[12]); 
  VVS3_0(w[12], w[8], w[0], pars->GC_319, amp[13]); 
  FFVS1_3_0(w[3], w[2], w[13], w[0], pars->GC_315, pars->GC_313, amp[14]); 
  VVS2_0(w[14], w[15], w[0], pars->GC_325, amp[15]); 
  VVS4_0(w[14], w[15], w[0], pars->GC_322, amp[16]); 
  VVS1_0(w[14], w[16], w[0], pars->GC_324, amp[17]); 
  VVS4_0(w[14], w[16], w[0], pars->GC_324, amp[18]); 
  VVS1_0(w[15], w[13], w[0], pars->GC_324, amp[19]); 
  VVS4_0(w[15], w[13], w[0], pars->GC_324, amp[20]); 
  VVS3_0(w[13], w[16], w[0], pars->GC_319, amp[21]); 
  VVS2_4_0(w[13], w[16], w[0], pars->GC_321, pars->GC_320, amp[22]); 
  VVS3_0(w[13], w[16], w[0], pars->GC_358, amp[23]); 
  VVS3_0(w[13], w[17], w[0], pars->GC_319, amp[24]); 
  VVS3_0(w[13], w[18], w[0], pars->GC_319, amp[25]); 
  VVS3_0(w[19], w[16], w[0], pars->GC_319, amp[26]); 
  VVS3_0(w[20], w[16], w[0], pars->GC_319, amp[27]); 
  FFVS1_3_0(w[1], w[4], w[16], w[0], pars->GC_315, pars->GC_313, amp[28]); 
  FFVS1_3_0(w[1], w[2], w[8], w[0], pars->GC_315, pars->GC_313, amp[29]); 

}
double CPPProcess::matrix_1_h_epemepem() 
{
  int i, j; 
  // Local variables
  const int ngraphs = 30; 
  const int ncolor = 1; 
  std::complex<double> ztemp; 
  std::complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1}; 
  static const double cf[ncolor][ncolor] = {{1}}; 

  // Calculate color flows
  jamp[0] = +amp[0] + amp[1] + amp[2] + amp[3] + amp[4] + amp[5] + amp[6] +
      amp[7] + amp[8] + amp[9] + amp[10] + amp[11] + amp[12] + amp[13] -
      amp[14] - amp[15] - amp[16] - amp[17] - amp[18] - amp[19] - amp[20] -
      amp[21] - amp[22] - amp[23] - amp[24] - amp[25] - amp[26] - amp[27] -
      amp[28] + amp[29];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])); 

  return matrix; 
}

}
