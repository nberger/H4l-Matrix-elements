A package to collect and run matrix element computations from madgraph.

Compiling and running
---------------------

The code should be compiled by simply running make in the top-level directory.

This creates an executable called MCExec that takes as input a root file (events.root by default) produced by running ExRootLHEFConverter on an (unzipped) LHE file. The output contains the ME information along with 4-vectors and the usual H->4l kinematic variable.

The rest of the processing is done through the scrpts in the macros/ directory:
1. marcros/analysis_cuts.C : applies cuts on dilepton masses, etc.
2. macros/hessian.C : computes the Hessian matrix of the EFT measurement and its eigenvectors
3. macros/plot_MLM.C : plots the impact of the EFT WCs and the EVs on the observables, and computes the OOs

Adding a ME computation
-----------------------

The matrix element code should be output from madgraph directly using 

output standalone_cpp CODE

the directory CODE should then be copied into H4l-Matrix-elements.

Unfortunately the MG code needs to be modified before it can be included:
1. The CPPProcess class must be into a namespace, to distinguish each ME from the other instances. This needs to be done in the CPPProcess.cc and CPPProcess.h files in each subprocess in CODE/SubProcesses. The convention is to use the name of the subprocess as namespace name (unless the same name is already in use)
2. The include guard (#ifndef ...) is unique to the MG model. If multiple MLMs using the same model are included, the include guards should be modified so they are still unique in each case. This also requires to rename the Parameters* and HelAmps* files, classes and namespaces in CODE/src ...
3. If multiple instances of an MLM are used with different parameters, need to replace Parameters_XXX::getInstance(); in CPPProcess.cc by new Parameters_XXX() to ensure each computation has a unique parameter set. (small memory leak...)

Once this is done, the computation can be added using the following steps:
1. Add CODE to the LIBS list in the Makefile
2. Add an MLM_CODE class in Base/src/ using one of the existing classes as template. The only changes should be the class name, the location of the CPPProcess include files (.cc and .h), the CPPProcess namespace name and the default param_card location.
3. Add a new ME variable in the ntuple by modifying src/MLMExec.cc:
   - add a new double, add it as a branch in the output TTree
   - add an instance of MLM_CODE to the code, alongside the others
   - fill the variable with the output MLM_Code::Eval and store in the TTree

EFT MLMs
--------

MLMExec includes a number of EFT variations, specified in the std::vector eftPars. Each one corresponds to a datacard in Cards/. These cards can be auto-generated from the template using the python script. So far this only sets a single parameter to 1 and all others to 0, but this can be adjusted.
