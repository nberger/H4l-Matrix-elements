#include <iostream>
#include <iomanip>

#include "TString.h"
#include "TLorentzVector.h"

#include "../../EFT_H4l/SubProcesses/P1_Sigma_SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless_h_epemepem/CPPProcess.h"
#include "../../EFT_H4l/SubProcesses/P1_Sigma_SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless_h_epemmupmum/CPPProcess.h"


class MLM_EFT_H4l
{
public:
  
  MLM_EFT_H4l(const TString& card = "EFT_H4l/Cards/param_card.dat")
  {
    cout << "setting card " << card << endl;
    process_eeee.initProc(card.Data());
    process_eemm.initProc(card.Data());
    cout << "done setting cards" << endl;
  }

  bool Eval(const std::vector<TLorentzVector>& p, double& mlm_eeee, double& mlm_eemm)
  {
    if (p.size() != 5) { cout << "ERROR: got a vector of size " << p.size() << " instead of the expected 5." << endl; return false; }
    std::vector<double*> fortran_p;
    for (unsigned int i = 0; i < p.size(); i++) {
      double* pp = new double[4];
      pp[0] = p[i].E();
      pp[1] = p[i].X();
      pp[2] = p[i].Y();
      pp[3] = p[i].Z();
      fortran_p.push_back(pp);
    }
    process_eeee.setMomenta(fortran_p);
    process_eeee.sigmaKin();
    const double* matrix_elements_eeee = process_eeee.getMatrixElements();
    mlm_eeee = matrix_elements_eeee[0];
    
    process_eemm.setMomenta(fortran_p);
    process_eemm.sigmaKin();
    const double* matrix_elements_eemm = process_eemm.getMatrixElements();
    mlm_eemm = matrix_elements_eemm[0];

    //for (unsigned int i = 0; i < fortran_p.size(); i++) delete fortran_p[i];
    return true;
  }
  
  double Eval_eeee(const std::vector<TLorentzVector>& p)
  {
    double eeee, eemm;
    Eval(p, eeee, eemm);
    return eeee;
  }

private:
  
  MG5_Sigma_SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless_h_epemepem  ::CPPProcess process_eeee;
  MG5_Sigma_SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless_h_epemmupmum::CPPProcess process_eemm;

};
