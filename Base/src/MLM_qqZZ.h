#include <iostream>
#include <iomanip>

#include "TString.h"
#include "TLorentzVector.h"

#include "../../qqZZ/SubProcesses/P1_Sigma_sm_uux_mupmummupmum/CPPProcess.h"
#include "../../qqZZ/SubProcesses/P1_Sigma_sm_ddx_mupmummupmum/CPPProcess.h"


class MLM_qqZZ
{
public:
  
  MLM_qqZZ(const TString& card = "qqZZ/Cards/param_card.dat")
  {
    cout << "setting card " << card << endl;
    process_uu.initProc(card.Data());
    process_dd.initProc(card.Data());
    cout << "done setting cards" << endl;
  }

  bool Eval(const std::vector<TLorentzVector>& p, double& mlm_uu, double& mlm_dd)
  {
    if (p.size() != 6) { cout << "ERROR: got a vector of size " << p.size() << " instead of the expected 5." << endl; return false; }
    std::vector<double*> fortran_p;
    for (unsigned int i = 0; i < p.size(); i++) {
      double* pp = new double[4];
      pp[0] = p[i].E();
      pp[1] = p[i].X();
      pp[2] = p[i].Y();
      pp[3] = p[i].Z();
      fortran_p.push_back(pp);
    }
    process_uu.setMomenta(fortran_p);
    process_uu.sigmaKin();
    const double* matrix_elements_uu = process_uu.getMatrixElements();
    mlm_uu = matrix_elements_uu[0];
    
    process_dd.setMomenta(fortran_p);
    process_dd.sigmaKin();
    const double* matrix_elements_dd = process_dd.getMatrixElements();
    mlm_dd = matrix_elements_dd[0];

    //for (unsigned int i = 0; i < fortran_p.size(); i++) delete fortran_p[i];
    return true;
  }
  
  double Eval_uu(const std::vector<TLorentzVector>& p)
  {
    double uu, dd;
    Eval(p, uu, dd);
    return uu;
  }

private:
  
  MG5_Sigma_sm_uux_mupmummupmum::CPPProcess process_uu;
  MG5_Sigma_sm_ddx_mupmummupmum::CPPProcess process_dd;

};
