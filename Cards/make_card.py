#! /usr/bin/python

# usage: make_card [par] where par is one of
# - SM    : output a card with all SMEFTsim parameters set to 0
# - [cXX] : output a card with cXX set to 1 and all other parameters set to 0
# - all   : output cards for all SMEFTsim parameters set to 1 in turn

import sys

def make_card(coeff) :
  
  params  = { 
    'ceWPh'     : 1e-99 , #  1
    'ceBPh'     : 1e-99 , #  2
    'cuGPh'     : 1e-99 , #  3
    'cuWPh'     : 1e-99 , #  4
    'cuBPh'     : 1e-99 , #  5
    'cdGPh'     : 1e-99 , #  6
    'cdWPh'     : 1e-99 , #  7
    'cdBPh'     : 1e-99 , #  8
    'cHudPh'    : 1e-99 , #  9
    'ceHPh'     : 1e-99 , # 10
    'cuHPh'     : 1e-99 , # 11
    'cdHPh'     : 1e-99 , # 12
    'cledqPh'   : 1e-99 , # 13
    'cquqd1Ph'  : 1e-99 , # 14
    'cquqd8Ph'  : 1e-99 , # 15
    'clequ1Ph'  : 1e-99 , # 16
    'clequ3Ph'  : 1e-99 , # 17
    'cG'        : 1e-99 , # 19
    'cGtil'     : 1e-99 , # 20
    'cW'        : 1e-99 , # 21
    'cWtil'     : 1e-99 , # 22
    'cH'        : 1e-99 , # 23
    'cHbox'     : 1e-99 , # 24
    'cHDD'      : 1e-99 , # 25
    'cHG'       : 1e-99 , # 26
    'cHGtil'    : 1e-99 , # 27
    'cHW'       : 1e-99 , # 28
    'cHWtil'    : 1e-99 , # 29
    'cHB'       : 1e-99 , # 30
    'cHBtil'    : 1e-99 , # 31
    'cHWB'      : 1e-99 , # 32
    'cHWBtil'   : 1e-99 , # 33
    'ceHAbs'    : 1e-99 , # 34
    'cuHAbs'    : 1e-99 , # 35
    'cdHAbs'    : 1e-99 , # 36
    'ceWAbs'    : 1e-99 , # 37
    'ceBAbs'    : 1e-99 , # 38
    'cuGAbs'    : 1e-99 , # 39
    'cuWAbs'    : 1e-99 , # 40
    'cuBAbs'    : 1e-99 , # 41
    'cdGAbs'    : 1e-99 , # 42
    'cdWAbs'    : 1e-99 , # 43
    'cdBAbs'    : 1e-99 , # 44
    'cHl1'      : 1e-99 , # 45
    'cHl3'      : 1e-99 , # 46
    'cHe'       : 1e-99 , # 47
    'cHq1'      : 1e-99 , # 48
    'cHq3'      : 1e-99 , # 49
    'cHu'       : 1e-99 , # 50
    'cHd'       : 1e-99 , # 51
    'cHudAbs'   : 1e-99 , # 52
    'cll'       : 1e-99 , # 53
    'cll1'      : 1e-99 , # 54
    'cqq1'      : 1e-99 , # 55
    'cqq11'     : 1e-99 , # 56
    'cqq3'      : 1e-99 , # 57
    'cqq31'     : 1e-99 , # 58
    'clq1'      : 1e-99 , # 59
    'clq3'      : 1e-99 , # 60
    'cee'       : 1e-99 , # 61
    'cuu'       : 1e-99 , # 62
    'cuu1'      : 1e-99 , # 63
    'cdd'       : 1e-99 , # 64
    'cdd1'      : 1e-99 , # 65
    'ceu'       : 1e-99 , # 66
    'ced'       : 1e-99 , # 67
    'cud1'      : 1e-99 , # 68
    'cud8'      : 1e-99 , # 69
    'cle'       : 1e-99 , # 70
    'clu'       : 1e-99 , # 71
    'cld'       : 1e-99 , # 72
    'cqe'       : 1e-99 , # 73
    'cqu1'      : 1e-99 , # 74
    'cqu8'      : 1e-99 , # 75
    'cqd1'      : 1e-99 , # 76
    'cqd8'      : 1e-99 , # 77
    'cledqAbs'  : 1e-99 , # 78
    'cquqd1Abs' : 1e-99 , # 79
    'cquqd8Abs' : 1e-99 , # 80
    'clequ1Abs' : 1e-99 , # 81
    'clequ3Abs' : 1e-99   # 82
    }
  
  order  = { 
    1 : 'ceWPh'     ,
    2 : 'ceBPh'     ,
    3 : 'cuGPh'     ,
    4 : 'cuWPh'     ,
    5 : 'cuBPh'     ,
    6 : 'cdGPh'     ,
    7 : 'cdWPh'     ,
    8 : 'cdBPh'     ,
    9 : 'cHudPh'    ,
    10 : 'ceHPh'     ,
    11 : 'cuHPh'     ,
    12 : 'cdHPh'     ,
    13 : 'cledqPh'   ,
    14 : 'cquqd1Ph'  ,
    15 : 'cquqd8Ph'  ,
    16 : 'clequ1Ph'  ,
    17 : 'clequ3Ph'  ,
    19 : 'cG'        ,
    20 : 'cGtil'     ,
    21 : 'cW'        ,
    22 : 'cWtil'     ,
    23 : 'cH'        ,
    24 : 'cHbox'     ,
    25 : 'cHDD'      ,
    26 : 'cHG'       ,
    27 : 'cHGtil'    ,
    28 : 'cHW'       ,
    29 : 'cHWtil'    ,
    30 : 'cHB'       ,
    31 : 'cHBtil'    ,
    32 : 'cHWB'      ,
    33 : 'cHWBtil'   ,
    34 : 'ceHAbs'    ,
    35 : 'cuHAbs'    ,
    36 : 'cdHAbs'    ,
    37 : 'ceWAbs'    ,
    38 : 'ceBAbs'    ,
    39 : 'cuGAbs'    ,
    40 : 'cuWAbs'    ,
    41 : 'cuBAbs'    ,
    42 : 'cdGAbs'    ,
    43 : 'cdWAbs'    ,
    44 : 'cdBAbs'    ,
    45 : 'cHl1'      ,
    46 : 'cHl3'      ,
    47 : 'cHe'       ,
    48 : 'cHq1'      ,
    49 : 'cHq3'      ,
    50 : 'cHu'       ,
    51 : 'cHd'       ,
    52 : 'cHudAbs'   ,
    53 : 'cll'       ,
    54 : 'cll1'      ,
    55 : 'cqq1'      ,
    56 : 'cqq11'     ,
    57 : 'cqq3'      ,
    58 : 'cqq31'     ,
    59 : 'clq1'      ,
    60 : 'clq3'      ,
    61 : 'cee'       ,
    62 : 'cuu'       ,
    63 : 'cuu1'      ,
    64 : 'cdd'       ,
    65 : 'cdd1'      ,
    66 : 'ceu'       ,
    67 : 'ced'       ,
    68 : 'cud1'      ,
    69 : 'cud8'      ,
    70 : 'cle'       ,
    71 : 'clu'       ,
    72 : 'cld'       ,
    73 : 'cqe'       ,
    74 : 'cqu1'      ,
    75 : 'cqu8'      ,
    76 : 'cqd1'      ,
    77 : 'cqd8'      ,
    78 : 'cledqAbs'  ,
    79 : 'cquqd1Abs' ,
    80 : 'cquqd8Abs' ,
    81 : 'clequ1Abs' ,
    82 : 'clequ3Abs'
      }
  
  indices = range(1,18) + range(19, 83)
  
  
  with open('SMEFTsim_massless_template.dat', 'r') as f :
    data = f.read()
    
    if coeff == 'all' :
      for p in params : make_card(p)
      return
    
    if coeff != "SM" :
      if not coeff in params:
        print 'ERROR: parameter %s is not a known SMEFTsim parameter' % coeff
        return
      params[coeff] = 1
      
    values = [ params[order[i]] for i in indices ]
    data_replaced = data % tuple(values)
      
    with open('SMEFTsim_massless_%s.dat' % coeff, 'w') as f :
      f.write(data_replaced)
    print 'Wrote card %s for parameter space point %s=1' % ('SMEFTsim_massless_%s.dat' % coeff, coeff)
      
make_card(sys.argv[1])
