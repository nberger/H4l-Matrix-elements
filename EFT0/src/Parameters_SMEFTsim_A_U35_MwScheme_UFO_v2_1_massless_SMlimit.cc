//==========================================================================
// This file has been automatically generated for C++ by
// MadGraph5_aMC@NLO v. 2.6.5, 2018-02-03
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include <iostream> 
#include <iomanip> 
#include "Parameters_SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless_SMlimit.h"

// Initialize static instance
Parameters_SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless_SMlimit *
    Parameters_SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless_SMlimit::instance = 0;

// Function to get static instance - only one instance per program
Parameters_SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless_SMlimit *
    Parameters_SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless_SMlimit::getInstance()
{
  if (instance == 0)
    instance = new Parameters_SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless_SMlimit(); 

  return instance; 
}

void Parameters_SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless_SMlimit::setIndependentParameters(SLHAReader& slha)
{
  // Define "zero"
  zero = 0; 
  ZERO = 0; 
  // Prepare a vector for indices
  vector<int> indices(2, 0); 
  mdl_WH = slha.get_block_entry("decay", 25, 4.070000e-03); 
  mdl_WW = slha.get_block_entry("decay", 24, 2.085000e+00); 
  mdl_WZ = slha.get_block_entry("decay", 23, 2.495200e+00); 
  mdl_WT = slha.get_block_entry("decay", 6, 1.508336e+00); 
  mdl_ymtau = slha.get_block_entry("yukawa", 15, 1.777000e+00); 
  mdl_ymt = slha.get_block_entry("yukawa", 6, 1.732000e+02); 
  mdl_ymb = slha.get_block_entry("yukawa", 5, 4.180000e+00); 
  mdl_ymdo = slha.get_block_entry("yukawa", 1, 1.000000e-99); 
  aS = slha.get_block_entry("sminputs", 3, 1.181000e-01); 
  mdl_Gf = slha.get_block_entry("sminputs", 2, 1.166379e-05); 
  mdl_MH = slha.get_block_entry("mass", 25, 1.250900e+02); 
  mdl_MZ = slha.get_block_entry("mass", 23, 9.118760e+01); 
  mdl_MTA = slha.get_block_entry("mass", 15, 1.777000e+00); 
  mdl_Me = slha.get_block_entry("mass", 11, 1.000000e-99); 
  mdl_MT = slha.get_block_entry("mass", 6, 1.732000e+02); 
  mdl_MB = slha.get_block_entry("mass", 5, 4.180000e+00); 
  mdl_MW0 = slha.get_block_entry("frblock", 83, 8.038700e+01); 
  mdl_clequ3Abs = slha.get_block_entry("frblock", 82, 1.000000e+00); 
  mdl_clequ1Abs = slha.get_block_entry("frblock", 81, 1.000000e+00); 
  mdl_cquqd8Abs = slha.get_block_entry("frblock", 80, 1.000000e+00); 
  mdl_cquqd1Abs = slha.get_block_entry("frblock", 79, 1.000000e+00); 
  mdl_cledqAbs = slha.get_block_entry("frblock", 78, 1.000000e+00); 
  mdl_cqd8 = slha.get_block_entry("frblock", 77, 1.000000e+00); 
  mdl_cqd1 = slha.get_block_entry("frblock", 76, 1.000000e+00); 
  mdl_cqu8 = slha.get_block_entry("frblock", 75, 1.000000e+00); 
  mdl_cqu1 = slha.get_block_entry("frblock", 74, 1.000000e+00); 
  mdl_cqe = slha.get_block_entry("frblock", 73, 1.000000e+00); 
  mdl_cld = slha.get_block_entry("frblock", 72, 1.000000e+00); 
  mdl_clu = slha.get_block_entry("frblock", 71, 1.000000e+00); 
  mdl_cle = slha.get_block_entry("frblock", 70, 1.000000e+00); 
  mdl_cud8 = slha.get_block_entry("frblock", 69, 1.000000e+00); 
  mdl_cud1 = slha.get_block_entry("frblock", 68, 1.000000e+00); 
  mdl_ced = slha.get_block_entry("frblock", 67, 1.000000e+00); 
  mdl_ceu = slha.get_block_entry("frblock", 66, 1.000000e+00); 
  mdl_cdd1 = slha.get_block_entry("frblock", 65, 1.000000e+00); 
  mdl_cdd = slha.get_block_entry("frblock", 64, 1.000000e+00); 
  mdl_cuu1 = slha.get_block_entry("frblock", 63, 1.000000e+00); 
  mdl_cuu = slha.get_block_entry("frblock", 62, 1.000000e+00); 
  mdl_cee = slha.get_block_entry("frblock", 61, 1.000000e+00); 
  mdl_clq3 = slha.get_block_entry("frblock", 60, 1.000000e+00); 
  mdl_clq1 = slha.get_block_entry("frblock", 59, 1.000000e+00); 
  mdl_cqq31 = slha.get_block_entry("frblock", 58, 1.000000e+00); 
  mdl_cqq3 = slha.get_block_entry("frblock", 57, 1.000000e+00); 
  mdl_cqq11 = slha.get_block_entry("frblock", 56, 1.000000e+00); 
  mdl_cqq1 = slha.get_block_entry("frblock", 55, 1.000000e+00); 
  mdl_cll1 = slha.get_block_entry("frblock", 54, 1.000000e+00); 
  mdl_cll = slha.get_block_entry("frblock", 53, 1.000000e+00); 
  mdl_cHudAbs = slha.get_block_entry("frblock", 52, 1.000000e+00); 
  mdl_cHd = slha.get_block_entry("frblock", 51, 1.000000e+00); 
  mdl_cHu = slha.get_block_entry("frblock", 50, 1.000000e+00); 
  mdl_cHq3 = slha.get_block_entry("frblock", 49, 1.000000e+00); 
  mdl_cHq1 = slha.get_block_entry("frblock", 48, 1.000000e+00); 
  mdl_cHe = slha.get_block_entry("frblock", 47, 1.000000e+00); 
  mdl_cHl3 = slha.get_block_entry("frblock", 46, 1.000000e+00); 
  mdl_cHl1 = slha.get_block_entry("frblock", 45, 1.000000e+00); 
  mdl_cdBAbs = slha.get_block_entry("frblock", 44, 1.000000e+00); 
  mdl_cdWAbs = slha.get_block_entry("frblock", 43, 1.000000e+00); 
  mdl_cdGAbs = slha.get_block_entry("frblock", 42, 1.000000e+00); 
  mdl_cuBAbs = slha.get_block_entry("frblock", 41, 1.000000e+00); 
  mdl_cuWAbs = slha.get_block_entry("frblock", 40, 1.000000e+00); 
  mdl_cuGAbs = slha.get_block_entry("frblock", 39, 1.000000e+00); 
  mdl_ceBAbs = slha.get_block_entry("frblock", 38, 1.000000e+00); 
  mdl_ceWAbs = slha.get_block_entry("frblock", 37, 1.000000e+00); 
  mdl_cdHAbs = slha.get_block_entry("frblock", 36, 1.000000e+00); 
  mdl_cuHAbs = slha.get_block_entry("frblock", 35, 1.000000e+00); 
  mdl_ceHAbs = slha.get_block_entry("frblock", 34, 1.000000e+00); 
  mdl_cHWBtil = slha.get_block_entry("frblock", 33, 1.000000e+00); 
  mdl_cHWB = slha.get_block_entry("frblock", 32, 1.000000e+00); 
  mdl_cHBtil = slha.get_block_entry("frblock", 31, 1.000000e+00); 
  mdl_cHB = slha.get_block_entry("frblock", 30, 1.000000e+00); 
  mdl_cHWtil = slha.get_block_entry("frblock", 29, 1.000000e+00); 
  mdl_cHW = slha.get_block_entry("frblock", 28, 1.000000e+00); 
  mdl_cHGtil = slha.get_block_entry("frblock", 27, 1.000000e+00); 
  mdl_cHG = slha.get_block_entry("frblock", 26, 1.000000e+00); 
  mdl_cHDD = slha.get_block_entry("frblock", 25, 1.000000e+00); 
  mdl_cHbox = slha.get_block_entry("frblock", 24, 1.000000e+00); 
  mdl_cH = slha.get_block_entry("frblock", 23, 1.000000e+00); 
  mdl_cWtil = slha.get_block_entry("frblock", 22, 1.000000e+00); 
  mdl_cW = slha.get_block_entry("frblock", 21, 1.000000e+00); 
  mdl_cGtil = slha.get_block_entry("frblock", 20, 1.000000e+00); 
  mdl_cG = slha.get_block_entry("frblock", 19, 1.000000e+00); 
  mdl_LambdaSMEFT = slha.get_block_entry("frblock", 18, 1.000000e+03); 
  mdl_ceWPh = slha.get_block_entry("frblock", 1, 1.000000e-99); 
  mdl_cabi = slha.get_block_entry("ckmblock", 1, 1.000000e-99); 
  mdl_MS = 1. * mdl_Me; 
  mdl_MD = 1. * mdl_Me; 
  mdl_MC = 1. * mdl_Me; 
  mdl_MU = 1. * mdl_Me; 
  mdl_MMU = 1. * mdl_Me; 
  mdl_ymm = 1. * mdl_ymdo; 
  mdl_yme = 1. * mdl_ymdo; 
  mdl_ymc = 1. * mdl_ymdo; 
  mdl_yms = 1. * mdl_ymdo; 
  mdl_ymup = 1. * mdl_ymdo; 
  mdl_clequ3Ph = 1. * mdl_ceWPh; 
  mdl_clequ1Ph = 1. * mdl_ceWPh; 
  mdl_cquqd8Ph = 1. * mdl_ceWPh; 
  mdl_cquqd1Ph = 1. * mdl_ceWPh; 
  mdl_cledqPh = 1. * mdl_ceWPh; 
  mdl_cdHPh = 1. * mdl_ceWPh; 
  mdl_cuHPh = 1. * mdl_ceWPh; 
  mdl_ceHPh = 1. * mdl_ceWPh; 
  mdl_cHudPh = 1. * mdl_ceWPh; 
  mdl_cdBPh = 1. * mdl_ceWPh; 
  mdl_cdWPh = 1. * mdl_ceWPh; 
  mdl_cdGPh = 1. * mdl_ceWPh; 
  mdl_cuBPh = 1. * mdl_ceWPh; 
  mdl_cuWPh = 1. * mdl_ceWPh; 
  mdl_cuGPh = 1. * mdl_ceWPh; 
  mdl_ceBPh = 1. * mdl_ceWPh; 
  mdl_CKMeta = 1. * mdl_cabi; 
  mdl_CKMrho = 1. * mdl_cabi; 
  mdl_CKMA = 1. * mdl_cabi; 
  mdl_CKMlambda = 1. * mdl_cabi; 
  mdl_conjg__CKM2x2 = 1.; 
  mdl_CKM1x1 = 1.; 
  mdl_conjg__CKM3x3 = 1.; 
  mdl_conjg__CKM1x1 = 1.; 
  mdl_CKM2x2 = 1.; 
  mdl_CKM3x3 = 1.; 
  mdl_complexi = std::complex<double> (0., 1.); 
  mdl_ceH = mdl_ceHAbs * exp(mdl_ceHPh * mdl_complexi); 
  mdl_cuH = mdl_cuHAbs * exp(mdl_cuHPh * mdl_complexi); 
  mdl_cdH = mdl_cdHAbs * exp(mdl_cdHPh * mdl_complexi); 
  mdl_ceW = mdl_ceWAbs * exp(mdl_ceWPh * mdl_complexi); 
  mdl_ceB = mdl_ceBAbs * exp(mdl_ceBPh * mdl_complexi); 
  mdl_cuG = mdl_cuGAbs * exp(mdl_cuGPh * mdl_complexi); 
  mdl_cuW = mdl_cuWAbs * exp(mdl_cuWPh * mdl_complexi); 
  mdl_cuB = mdl_cuBAbs * exp(mdl_cuBPh * mdl_complexi); 
  mdl_cdG = mdl_cdGAbs * exp(mdl_cdGPh * mdl_complexi); 
  mdl_cdW = mdl_cdWAbs * exp(mdl_cdWPh * mdl_complexi); 
  mdl_cdB = mdl_cdBAbs * exp(mdl_cdBPh * mdl_complexi); 
  mdl_cHud = mdl_cHudAbs * exp(mdl_cHudPh * mdl_complexi); 
  mdl_cledq = mdl_cledqAbs * exp(mdl_cledqPh * mdl_complexi); 
  mdl_cquqd1 = mdl_cquqd1Abs * exp(mdl_cquqd1Ph * mdl_complexi); 
  mdl_cquqd8 = mdl_cquqd8Abs * exp(mdl_cquqd8Ph * mdl_complexi); 
  mdl_clequ1 = mdl_clequ1Abs * exp(mdl_clequ1Ph * mdl_complexi); 
  mdl_clequ3 = mdl_clequ3Abs * exp(mdl_clequ3Ph * mdl_complexi); 
  mdl_MW = mdl_MW0; 
  mdl_nb__2__exp__0_25 = pow(2., 0.25); 
  mdl_MH__exp__2 = ((mdl_MH) * (mdl_MH)); 
  mdl_sqrt__2 = sqrt(2.); 
  mdl_CKMlambda__exp__2 = ((mdl_CKMlambda) * (mdl_CKMlambda)); 
  mdl_CKM1x2 = mdl_CKMlambda; 
  mdl_CKMlambda__exp__3 = ((mdl_CKMlambda) * (mdl_CKMlambda) *
      (mdl_CKMlambda));
  mdl_CKM2x1 = -mdl_CKMlambda; 
  mdl_CKM2x3 = mdl_CKMA * mdl_CKMlambda__exp__2; 
  mdl_CKM3x2 = -(mdl_CKMA * mdl_CKMlambda__exp__2); 
  mdl_MW__exp__2 = ((mdl_MW) * (mdl_MW)); 
  mdl_MZ__exp__2 = ((mdl_MZ) * (mdl_MZ)); 
  mdl_sth2 = 1. - mdl_MW__exp__2/mdl_MZ__exp__2; 
  mdl_LambdaSMEFT__exp__2 = ((mdl_LambdaSMEFT) * (mdl_LambdaSMEFT)); 
  mdl_MH__exp__6 = pow(mdl_MH, 6.); 
  mdl_MT__exp__6 = pow(mdl_MT, 6.); 
  mdl_MH__exp__4 = ((mdl_MH) * (mdl_MH) * (mdl_MH) * (mdl_MH)); 
  mdl_MT__exp__4 = ((mdl_MT) * (mdl_MT) * (mdl_MT) * (mdl_MT)); 
  mdl_MT__exp__2 = ((mdl_MT) * (mdl_MT)); 
  mdl_cth = sqrt(1. - mdl_sth2); 
  mdl_sqrt__sth2 = sqrt(mdl_sth2); 
  mdl_sth = mdl_sqrt__sth2; 
  mdl_MW0__exp__6 = pow(mdl_MW0, 6.); 
  mdl_MW0__exp__4 = ((mdl_MW0) * (mdl_MW0) * (mdl_MW0) * (mdl_MW0)); 
  mdl_MW0__exp__2 = ((mdl_MW0) * (mdl_MW0)); 
  mdl_MZ__exp__4 = ((mdl_MZ) * (mdl_MZ) * (mdl_MZ) * (mdl_MZ)); 
  mdl_MZ__exp__6 = pow(mdl_MZ, 6.); 
  mdl_cth__exp__2 = ((mdl_cth) * (mdl_cth)); 
  mdl_sth__exp__2 = ((mdl_sth) * (mdl_sth)); 
  mdl_conjg__CKM2x1 = conj(mdl_CKM2x1); 
  mdl_conjg__CKM1x2 = conj(mdl_CKM1x2); 
  mdl_conjg__CKM3x2 = conj(mdl_CKM3x2); 
  mdl_conjg__CKM2x3 = conj(mdl_CKM2x3); 
  mdl_cth__exp__4 = ((mdl_cth) * (mdl_cth) * (mdl_cth) * (mdl_cth)); 
  mdl_cth__exp__3 = ((mdl_cth) * (mdl_cth) * (mdl_cth)); 
  mdl_sth__exp__3 = ((mdl_sth) * (mdl_sth) * (mdl_sth)); 
  mdl_sth__exp__4 = ((mdl_sth) * (mdl_sth) * (mdl_sth) * (mdl_sth)); 
  mdl_LambdaSMEFT__exp__4 = ((mdl_LambdaSMEFT) * (mdl_LambdaSMEFT) *
      (mdl_LambdaSMEFT) * (mdl_LambdaSMEFT));
  mdl_conjg__cdG = conj(mdl_cdG); 
  mdl_conjg__cdH = conj(mdl_cdH); 
  mdl_conjg__cdW = conj(mdl_cdW); 
  mdl_conjg__cdB = conj(mdl_cdB); 
  mdl_conjg__ceH = conj(mdl_ceH); 
  mdl_conjg__ceW = conj(mdl_ceW); 
  mdl_conjg__ceB = conj(mdl_ceB); 
  mdl_conjg__cHud = conj(mdl_cHud); 
  mdl_conjg__cledq = conj(mdl_cledq); 
  mdl_conjg__clequ1 = conj(mdl_clequ1); 
  mdl_conjg__clequ3 = conj(mdl_clequ3); 
  mdl_conjg__cquqd1 = conj(mdl_cquqd1); 
  mdl_conjg__cquqd8 = conj(mdl_cquqd8); 
  mdl_conjg__cuG = conj(mdl_cuG); 
  mdl_conjg__cuH = conj(mdl_cuH); 
  mdl_conjg__cuW = conj(mdl_cuW); 
  mdl_conjg__cuB = conj(mdl_cuB); 
  mdl_sqrt__Gf = sqrt(mdl_Gf); 
  mdl_vevhat = 1./(mdl_nb__2__exp__0_25 * mdl_sqrt__Gf); 
  mdl_lam = (mdl_Gf * mdl_MH__exp__2)/mdl_sqrt__2; 
  mdl_aEW = (mdl_Gf * mdl_MW__exp__2 * (1. - mdl_MW__exp__2/mdl_MZ__exp__2) *
      mdl_sqrt__2)/M_PI;
  mdl_vevhat__exp__2 = ((mdl_vevhat) * (mdl_vevhat)); 
  mdl_dGf = (mdl_vevhat__exp__2 * (-(mdl_cll1/mdl_sqrt__2) + mdl_cHl3 *
      mdl_sqrt__2))/mdl_LambdaSMEFT__exp__2;
  mdl_dMH2 = ((2. * mdl_cHbox - mdl_cHDD/2. - (3. * mdl_cH)/(2. * mdl_lam)) *
      mdl_MH__exp__2 * mdl_vevhat__exp__2)/mdl_LambdaSMEFT__exp__2;
  mdl_yb = (mdl_ymb * mdl_sqrt__2)/mdl_vevhat; 
  mdl_yc = (mdl_ymc * mdl_sqrt__2)/mdl_vevhat; 
  mdl_ydo = (mdl_ymdo * mdl_sqrt__2)/mdl_vevhat; 
  mdl_ye = (mdl_yme * mdl_sqrt__2)/mdl_vevhat; 
  mdl_ym = (mdl_ymm * mdl_sqrt__2)/mdl_vevhat; 
  mdl_ys = (mdl_yms * mdl_sqrt__2)/mdl_vevhat; 
  mdl_yt = (mdl_ymt * mdl_sqrt__2)/mdl_vevhat; 
  mdl_ytau = (mdl_ymtau * mdl_sqrt__2)/mdl_vevhat; 
  mdl_yup = (mdl_ymup * mdl_sqrt__2)/mdl_vevhat; 
  mdl_barlam = mdl_lam * (1. - mdl_dMH2/mdl_MH__exp__2 - mdl_dGf *
      mdl_sqrt__2);
  mdl_vevT = mdl_vevhat * (1. + mdl_dGf/mdl_sqrt__2); 
  mdl_dgw = -(mdl_dGf/mdl_sqrt__2); 
  mdl_sqrt__aEW = sqrt(mdl_aEW); 
  mdl_ee = 2. * mdl_sqrt__aEW * sqrt(M_PI); 
  mdl_gwsh = (mdl_ee * (1. + mdl_dgw - (mdl_cHW *
      mdl_vevhat__exp__2)/mdl_LambdaSMEFT__exp__2))/mdl_sth;
  mdl_vev = (1. - (3. * mdl_cH * mdl_vevhat__exp__2)/(8. * mdl_lam *
      mdl_LambdaSMEFT__exp__2)) * mdl_vevT;
  mdl_dMZ2 = (mdl_MZ__exp__2 * (mdl_cHDD/2. + 2. * mdl_cHWB * mdl_cth *
      mdl_sth) * mdl_vevhat__exp__2)/mdl_LambdaSMEFT__exp__2;
  mdl_g1 = mdl_ee/mdl_cth; 
  mdl_gw = mdl_ee/mdl_sth; 
  mdl_ee__exp__2 = ((mdl_ee) * (mdl_ee)); 
  mdl_gHaa = (mdl_ee__exp__2 * (-1.75 + (4. * (0.3333333333333333 + (13. *
      mdl_MH__exp__6)/(50400. * mdl_MT__exp__6) + mdl_MH__exp__4/(504. *
      mdl_MT__exp__4) + (7. * mdl_MH__exp__2)/(360. * mdl_MT__exp__2)))/3. -
      (29. * mdl_MH__exp__6)/(16800. * mdl_MW0__exp__6) - (19. *
      mdl_MH__exp__4)/(1680. * mdl_MW0__exp__4) - (11. * mdl_MH__exp__2)/(120.
      * mdl_MW0__exp__2)))/(8. * ((M_PI) * (M_PI)));
  mdl_gHza = (mdl_ee__exp__2 * (((0.4583333333333333 + (29. *
      mdl_MH__exp__6)/(100800. * mdl_MW0__exp__6) + (19. *
      mdl_MH__exp__4)/(10080. * mdl_MW0__exp__4) + (11. * mdl_MH__exp__2)/(720.
      * mdl_MW0__exp__2) + (mdl_MH__exp__4 * mdl_MZ__exp__2)/(2100. *
      mdl_MW0__exp__6) + (mdl_MH__exp__2 * mdl_MZ__exp__2)/(280. *
      mdl_MW0__exp__4) + (7. * mdl_MZ__exp__2)/(180. * mdl_MW0__exp__2) + (67.
      * mdl_MH__exp__2 * mdl_MZ__exp__4)/(100800. * mdl_MW0__exp__6) + (53. *
      mdl_MZ__exp__4)/(10080. * mdl_MW0__exp__4) + (43. *
      mdl_MZ__exp__6)/(50400. * mdl_MW0__exp__6) - (31. * mdl_cth__exp__2)/(24.
      * mdl_sth__exp__2) - (29. * mdl_cth__exp__2 * mdl_MH__exp__6)/(20160. *
      mdl_MW0__exp__6 * mdl_sth__exp__2) - (19. * mdl_cth__exp__2 *
      mdl_MH__exp__4)/(2016. * mdl_MW0__exp__4 * mdl_sth__exp__2) - (11. *
      mdl_cth__exp__2 * mdl_MH__exp__2)/(144. * mdl_MW0__exp__2 *
      mdl_sth__exp__2) - (mdl_cth__exp__2 * mdl_MH__exp__4 *
      mdl_MZ__exp__2)/(560. * mdl_MW0__exp__6 * mdl_sth__exp__2) - (31. *
      mdl_cth__exp__2 * mdl_MH__exp__2 * mdl_MZ__exp__2)/(2520. *
      mdl_MW0__exp__4 * mdl_sth__exp__2) - (mdl_cth__exp__2 *
      mdl_MZ__exp__2)/(9. * mdl_MW0__exp__2 * mdl_sth__exp__2) - (43. *
      mdl_cth__exp__2 * mdl_MH__exp__2 * mdl_MZ__exp__4)/(20160. *
      mdl_MW0__exp__6 * mdl_sth__exp__2) - (17. * mdl_cth__exp__2 *
      mdl_MZ__exp__4)/(1120. * mdl_MW0__exp__4 * mdl_sth__exp__2) - (5. *
      mdl_cth__exp__2 * mdl_MZ__exp__6)/(2016. * mdl_MW0__exp__6 *
      mdl_sth__exp__2)) * mdl_sth)/mdl_cth + ((0.3333333333333333 + (13. *
      mdl_MH__exp__6)/(50400. * mdl_MT__exp__6) + mdl_MH__exp__4/(504. *
      mdl_MT__exp__4) + (7. * mdl_MH__exp__2)/(360. * mdl_MT__exp__2) +
      (mdl_MH__exp__4 * mdl_MZ__exp__2)/(2400. * mdl_MT__exp__6) +
      (mdl_MH__exp__2 * mdl_MZ__exp__2)/(315. * mdl_MT__exp__4) + (11. *
      mdl_MZ__exp__2)/(360. * mdl_MT__exp__2) + (29. * mdl_MH__exp__2 *
      mdl_MZ__exp__4)/(50400. * mdl_MT__exp__6) + (11. * mdl_MZ__exp__4)/(2520.
      * mdl_MT__exp__4) + (37. * mdl_MZ__exp__6)/(50400. * mdl_MT__exp__6)) *
      (0.5 - (4. * mdl_sth__exp__2)/3.))/(mdl_cth * mdl_sth)))/(4. * ((M_PI) *
      (M_PI)));
  mdl_dg1 = (-(mdl_dMZ2/(mdl_MZ__exp__2 * mdl_sth__exp__2)) - mdl_dGf *
      mdl_sqrt__2)/2.;
  mdl_g1sh = (mdl_ee * (1. + mdl_dg1 - (mdl_cHB *
      mdl_vevhat__exp__2)/mdl_LambdaSMEFT__exp__2))/mdl_cth;
  mdl_dsth2 = 2. * mdl_cth__exp__2 * (mdl_dg1 - mdl_dgw) * mdl_sth__exp__2 +
      (mdl_cHWB * mdl_cth * mdl_sth * (1. - 2. * mdl_sth__exp__2) *
      mdl_vevhat__exp__2)/mdl_LambdaSMEFT__exp__2;
  mdl_I1b11 = mdl_CKM1x1 * mdl_ydo; 
  mdl_I1b12 = mdl_CKM2x1 * mdl_ydo; 
  mdl_I1b21 = mdl_CKM1x2 * mdl_ys; 
  mdl_I1b22 = mdl_CKM2x2 * mdl_ys; 
  mdl_I1b32 = mdl_CKM2x3 * mdl_yb; 
  mdl_I1b33 = mdl_CKM3x3 * mdl_yb; 
  mdl_I2b11 = mdl_CKM1x1 * mdl_yup; 
  mdl_I2b12 = mdl_CKM2x1 * mdl_yc; 
  mdl_I2b21 = mdl_CKM1x2 * mdl_yup; 
  mdl_I2b22 = mdl_CKM2x2 * mdl_yc; 
  mdl_I2b23 = mdl_CKM3x2 * mdl_yt; 
  mdl_I2b33 = mdl_CKM3x3 * mdl_yt; 
  mdl_I3b11 = mdl_ydo * mdl_conjg__CKM1x1; 
  mdl_I3b12 = mdl_ydo * mdl_conjg__CKM2x1; 
  mdl_I3b21 = mdl_ys * mdl_conjg__CKM1x2; 
  mdl_I3b22 = mdl_ys * mdl_conjg__CKM2x2; 
  mdl_I3b32 = mdl_yb * mdl_conjg__CKM2x3; 
  mdl_I3b33 = mdl_yb * mdl_conjg__CKM3x3; 
  mdl_I4b11 = mdl_yup * mdl_conjg__CKM1x1; 
  mdl_I4b12 = mdl_yc * mdl_conjg__CKM2x1; 
  mdl_I4b21 = mdl_yup * mdl_conjg__CKM1x2; 
  mdl_I4b22 = mdl_yc * mdl_conjg__CKM2x2; 
  mdl_I4b23 = mdl_yt * mdl_conjg__CKM3x2; 
  mdl_I4b33 = mdl_yt * mdl_conjg__CKM3x3; 
  mdl_I5b11 = mdl_ydo * mdl_conjg__CKM1x1; 
  mdl_I5b12 = mdl_ydo * mdl_conjg__CKM2x1; 
  mdl_I5b21 = mdl_ys * mdl_conjg__CKM1x2; 
  mdl_I5b22 = mdl_ys * mdl_conjg__CKM2x2; 
  mdl_I5b32 = mdl_yb * mdl_conjg__CKM2x3; 
  mdl_I5b33 = mdl_yb * mdl_conjg__CKM3x3; 
  mdl_I6b11 = mdl_yup * mdl_conjg__CKM1x1; 
  mdl_I6b12 = mdl_yc * mdl_conjg__CKM2x1; 
  mdl_I6b21 = mdl_yup * mdl_conjg__CKM1x2; 
  mdl_I6b22 = mdl_yc * mdl_conjg__CKM2x2; 
  mdl_I6b23 = mdl_yt * mdl_conjg__CKM3x2; 
  mdl_I6b33 = mdl_yt * mdl_conjg__CKM3x3; 
  mdl_I7b11 = mdl_CKM1x1 * mdl_ydo * mdl_yup; 
  mdl_I7b12 = mdl_CKM1x2 * mdl_ys * mdl_yup; 
  mdl_I7b21 = mdl_CKM2x1 * mdl_yc * mdl_ydo; 
  mdl_I7b22 = mdl_CKM2x2 * mdl_yc * mdl_ys; 
  mdl_I7b33 = mdl_CKM3x3 * mdl_yb * mdl_yt; 
  mdl_I8b11 = mdl_ydo * mdl_yup * mdl_conjg__CKM1x1; 
  mdl_I8b12 = mdl_yc * mdl_ydo * mdl_conjg__CKM2x1; 
  mdl_I8b21 = mdl_ys * mdl_yup * mdl_conjg__CKM1x2; 
  mdl_I8b22 = mdl_yc * mdl_ys * mdl_conjg__CKM2x2; 
  mdl_I8b33 = mdl_yb * mdl_yt * mdl_conjg__CKM3x3; 
  mdl_vevhat__exp__3 = ((mdl_vevhat) * (mdl_vevhat) * (mdl_vevhat)); 
  mdl_vevhat__exp__4 = ((mdl_vevhat) * (mdl_vevhat) * (mdl_vevhat) *
      (mdl_vevhat));
}
void Parameters_SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless_SMlimit::setIndependentCouplings()
{
  GC_3 = -(mdl_ee * mdl_complexi); 
  GC_234 = -(mdl_cth * mdl_ee * mdl_complexi)/(2. * mdl_sth); 
  GC_249 = -(mdl_ee * mdl_complexi * mdl_sth)/(2. * mdl_cth); 
  GC_313 = (mdl_cHe * mdl_cth * mdl_ee * mdl_complexi *
      mdl_vevhat)/(mdl_LambdaSMEFT__exp__2 * mdl_sth) + (mdl_cHe * mdl_ee *
      mdl_complexi * mdl_sth * mdl_vevhat)/(mdl_cth * mdl_LambdaSMEFT__exp__2);
  GC_315 = (mdl_cHl1 * mdl_cth * mdl_ee * mdl_complexi *
      mdl_vevhat)/(mdl_LambdaSMEFT__exp__2 * mdl_sth) + (mdl_cHl3 * mdl_cth *
      mdl_ee * mdl_complexi * mdl_vevhat)/(mdl_LambdaSMEFT__exp__2 * mdl_sth) +
      (mdl_cHl1 * mdl_ee * mdl_complexi * mdl_sth * mdl_vevhat)/(mdl_cth *
      mdl_LambdaSMEFT__exp__2) + (mdl_cHl3 * mdl_ee * mdl_complexi * mdl_sth *
      mdl_vevhat)/(mdl_cth * mdl_LambdaSMEFT__exp__2);
  GC_319 = mdl_ee__exp__2 * mdl_complexi * mdl_vevhat + (mdl_cth__exp__2 *
      mdl_ee__exp__2 * mdl_complexi * mdl_vevhat)/(2. * mdl_sth__exp__2) +
      (mdl_ee__exp__2 * mdl_complexi * mdl_sth__exp__2 * mdl_vevhat)/(2. *
      mdl_cth__exp__2);
  GC_320 = (4. * mdl_cHW * mdl_cth__exp__2 * mdl_complexi *
      mdl_vevhat)/mdl_LambdaSMEFT__exp__2 + (4. * mdl_cHWB * mdl_cth *
      mdl_complexi * mdl_sth * mdl_vevhat)/mdl_LambdaSMEFT__exp__2 + (4. *
      mdl_cHB * mdl_complexi * mdl_sth__exp__2 *
      mdl_vevhat)/mdl_LambdaSMEFT__exp__2;
  GC_321 = (-2. * mdl_cHWtil * mdl_cth__exp__2 * mdl_complexi *
      mdl_vevhat)/mdl_LambdaSMEFT__exp__2 - (2. * mdl_cHWBtil * mdl_cth *
      mdl_complexi * mdl_sth * mdl_vevhat)/mdl_LambdaSMEFT__exp__2 - (2. *
      mdl_cHBtil * mdl_complexi * mdl_sth__exp__2 *
      mdl_vevhat)/mdl_LambdaSMEFT__exp__2;
  GC_322 = (4. * mdl_cHB * mdl_cth__exp__2 * mdl_complexi *
      mdl_vevhat)/mdl_LambdaSMEFT__exp__2 - (4. * mdl_cHWB * mdl_cth *
      mdl_complexi * mdl_sth * mdl_vevhat)/mdl_LambdaSMEFT__exp__2 + (4. *
      mdl_cHW * mdl_complexi * mdl_sth__exp__2 *
      mdl_vevhat)/mdl_LambdaSMEFT__exp__2;
  GC_324 = (-2. * mdl_cHWBtil * mdl_cth__exp__2 * mdl_complexi *
      mdl_vevhat)/mdl_LambdaSMEFT__exp__2 - (4. * mdl_cHBtil * mdl_cth *
      mdl_complexi * mdl_sth * mdl_vevhat)/mdl_LambdaSMEFT__exp__2 + (4. *
      mdl_cHWtil * mdl_cth * mdl_complexi * mdl_sth *
      mdl_vevhat)/mdl_LambdaSMEFT__exp__2 + (2. * mdl_cHWBtil * mdl_complexi *
      mdl_sth__exp__2 * mdl_vevhat)/mdl_LambdaSMEFT__exp__2;
  GC_325 = (-2. * mdl_cHBtil * mdl_cth__exp__2 * mdl_complexi *
      mdl_vevhat)/mdl_LambdaSMEFT__exp__2 + (2. * mdl_cHWBtil * mdl_cth *
      mdl_complexi * mdl_sth * mdl_vevhat)/mdl_LambdaSMEFT__exp__2 - (2. *
      mdl_cHWtil * mdl_complexi * mdl_sth__exp__2 *
      mdl_vevhat)/mdl_LambdaSMEFT__exp__2;
  GC_328 = -(mdl_dsth2 * mdl_ee * mdl_complexi)/(2. * mdl_cth * mdl_sth) -
      (mdl_cHWB * mdl_ee * mdl_complexi * mdl_vevhat__exp__2)/(2. *
      mdl_LambdaSMEFT__exp__2);
  GC_337 = (mdl_cHu * mdl_cth * mdl_ee * mdl_complexi * mdl_vevhat__exp__2)/(2.
      * mdl_LambdaSMEFT__exp__2 * mdl_sth) + (mdl_cHu * mdl_ee * mdl_complexi *
      mdl_sth * mdl_vevhat__exp__2)/(2. * mdl_cth * mdl_LambdaSMEFT__exp__2);
  GC_344 = (mdl_cth * mdl_dgw * mdl_ee * mdl_complexi)/(2. * mdl_sth) +
      (mdl_cHWB * mdl_cth__exp__2 * mdl_ee * mdl_complexi *
      mdl_vevhat__exp__2)/(2. * mdl_LambdaSMEFT__exp__2) + (mdl_cHq1 * mdl_cth
      * mdl_ee * mdl_complexi * mdl_vevhat__exp__2)/(2. *
      mdl_LambdaSMEFT__exp__2 * mdl_sth) + (mdl_cHq3 * mdl_cth * mdl_ee *
      mdl_complexi * mdl_vevhat__exp__2)/(2. * mdl_LambdaSMEFT__exp__2 *
      mdl_sth) - (mdl_cHW * mdl_cth * mdl_ee * mdl_complexi *
      mdl_vevhat__exp__2)/(2. * mdl_LambdaSMEFT__exp__2 * mdl_sth) + (mdl_cHW *
      mdl_cth__exp__3 * mdl_ee * mdl_complexi * mdl_vevhat__exp__2)/(2. *
      mdl_LambdaSMEFT__exp__2 * mdl_sth) + (mdl_cHq1 * mdl_ee * mdl_complexi *
      mdl_sth * mdl_vevhat__exp__2)/(2. * mdl_cth * mdl_LambdaSMEFT__exp__2) +
      (mdl_cHq3 * mdl_ee * mdl_complexi * mdl_sth * mdl_vevhat__exp__2)/(2. *
      mdl_cth * mdl_LambdaSMEFT__exp__2) + (mdl_cHW * mdl_cth * mdl_ee *
      mdl_complexi * mdl_sth * mdl_vevhat__exp__2)/(2. *
      mdl_LambdaSMEFT__exp__2) + (mdl_cHWB * mdl_ee * mdl_complexi *
      mdl_sth__exp__2 * mdl_vevhat__exp__2)/(2. * mdl_LambdaSMEFT__exp__2);
  GC_349 = -(mdl_dg1 * mdl_ee * mdl_complexi * mdl_sth)/(2. * mdl_cth) +
      (mdl_cHB * mdl_ee * mdl_complexi * mdl_sth * mdl_vevhat__exp__2)/(2. *
      mdl_cth * mdl_LambdaSMEFT__exp__2) - (mdl_cHB * mdl_cth * mdl_ee *
      mdl_complexi * mdl_sth * mdl_vevhat__exp__2)/(2. *
      mdl_LambdaSMEFT__exp__2) - (mdl_cHB * mdl_ee * mdl_complexi *
      mdl_sth__exp__3 * mdl_vevhat__exp__2)/(2. * mdl_cth *
      mdl_LambdaSMEFT__exp__2);
  GC_358 = mdl_dg1 * mdl_ee__exp__2 * mdl_complexi * mdl_vevhat + mdl_dgw *
      mdl_ee__exp__2 * mdl_complexi * mdl_vevhat + (mdl_cth__exp__2 * mdl_dgw *
      mdl_ee__exp__2 * mdl_complexi * mdl_vevhat)/mdl_sth__exp__2 + (mdl_dg1 *
      mdl_ee__exp__2 * mdl_complexi * mdl_sth__exp__2 *
      mdl_vevhat)/mdl_cth__exp__2 - (mdl_cHB * mdl_ee__exp__2 * mdl_complexi *
      mdl_vevhat__exp__3)/mdl_LambdaSMEFT__exp__2 + (mdl_cHbox * mdl_ee__exp__2
      * mdl_complexi * mdl_vevhat__exp__3)/mdl_LambdaSMEFT__exp__2 + (3. *
      mdl_cHDD * mdl_ee__exp__2 * mdl_complexi * mdl_vevhat__exp__3)/(4. *
      mdl_LambdaSMEFT__exp__2) - (mdl_cHW * mdl_ee__exp__2 * mdl_complexi *
      mdl_vevhat__exp__3)/mdl_LambdaSMEFT__exp__2 + (mdl_cHB * mdl_cth__exp__2
      * mdl_ee__exp__2 * mdl_complexi * mdl_vevhat__exp__3)/mdl_LambdaSMEFT__exp__2 + (2. * mdl_cHW * mdl_cth__exp__2 * mdl_ee__exp__2 * mdl_complexi * mdl_vevhat__exp__3)/mdl_LambdaSMEFT__exp__2 + (mdl_cHbox * mdl_cth__exp__2 * mdl_ee__exp__2 * mdl_complexi * mdl_vevhat__exp__3)/(2. * mdl_LambdaSMEFT__exp__2 * mdl_sth__exp__2) + (3. * mdl_cHDD * mdl_cth__exp__2 * mdl_ee__exp__2 * mdl_complexi * mdl_vevhat__exp__3)/(8. * mdl_LambdaSMEFT__exp__2 * mdl_sth__exp__2) - (mdl_cHW * mdl_cth__exp__2 * mdl_ee__exp__2 * mdl_complexi * mdl_vevhat__exp__3)/(mdl_LambdaSMEFT__exp__2 * mdl_sth__exp__2) + (mdl_cHW * mdl_cth__exp__4 * mdl_ee__exp__2 * mdl_complexi * mdl_vevhat__exp__3)/(mdl_LambdaSMEFT__exp__2 * mdl_sth__exp__2) + (mdl_cHWB * mdl_cth__exp__3 * mdl_ee__exp__2 * mdl_complexi * mdl_vevhat__exp__3)/(mdl_LambdaSMEFT__exp__2 * mdl_sth) + (2. * mdl_cHWB * mdl_cth * mdl_ee__exp__2 * mdl_complexi * mdl_sth * mdl_vevhat__exp__3)/mdl_LambdaSMEFT__exp__2 + (2. * mdl_cHB * mdl_ee__exp__2 * mdl_complexi * mdl_sth__exp__2 * mdl_vevhat__exp__3)/mdl_LambdaSMEFT__exp__2 + (mdl_cHW * mdl_ee__exp__2 * mdl_complexi * mdl_sth__exp__2 * mdl_vevhat__exp__3)/mdl_LambdaSMEFT__exp__2 - (mdl_cHB * mdl_ee__exp__2 * mdl_complexi * mdl_sth__exp__2 * mdl_vevhat__exp__3)/(mdl_cth__exp__2 * mdl_LambdaSMEFT__exp__2) + (mdl_cHbox * mdl_ee__exp__2 * mdl_complexi * mdl_sth__exp__2 * mdl_vevhat__exp__3)/(2. * mdl_cth__exp__2 * mdl_LambdaSMEFT__exp__2) + (3. * mdl_cHDD * mdl_ee__exp__2 * mdl_complexi * mdl_sth__exp__2 * mdl_vevhat__exp__3)/(8. * mdl_cth__exp__2 * mdl_LambdaSMEFT__exp__2) + (mdl_cHWB * mdl_ee__exp__2 * mdl_complexi * mdl_sth__exp__3 * mdl_vevhat__exp__3)/(mdl_cth * mdl_LambdaSMEFT__exp__2) + (mdl_cHB * mdl_ee__exp__2 * mdl_complexi * mdl_sth__exp__4 * mdl_vevhat__exp__3)/(mdl_cth__exp__2 * mdl_LambdaSMEFT__exp__2) + (mdl_dGf * mdl_ee__exp__2 * mdl_complexi * mdl_vevhat)/mdl_sqrt__2 + (mdl_cth__exp__2 * mdl_dGf * mdl_ee__exp__2 * mdl_complexi * mdl_vevhat)/(2. * mdl_sth__exp__2 * mdl_sqrt__2) + (mdl_dGf * mdl_ee__exp__2 * mdl_complexi * mdl_sth__exp__2 * mdl_vevhat)/(2. * mdl_cth__exp__2 * mdl_sqrt__2);
}
void Parameters_SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless_SMlimit::setDependentParameters()
{
  mdl_sqrt__aS = sqrt(aS); 
  G = 2. * mdl_sqrt__aS * sqrt(M_PI); 
  mdl_G__exp__2 = ((G) * (G)); 
  mdl_gHgg = (mdl_G__exp__2 * (0.3333333333333333 + (13. *
      mdl_MH__exp__6)/(50400. * mdl_MT__exp__6) + mdl_MH__exp__4/(504. *
      mdl_MT__exp__4) + (7. * mdl_MH__exp__2)/(360. * mdl_MT__exp__2)))/(16. *
      ((M_PI) * (M_PI)));
}
void Parameters_SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless_SMlimit::setDependentCouplings()
{

}

// Routines for printing out parameters
void Parameters_SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless_SMlimit::printIndependentParameters()
{
  cout <<  "SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless_SMlimit model parameters independent of event kinematics:" <<
      endl;
  cout << setw(20) <<  "mdl_WH " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_WH << endl;
  cout << setw(20) <<  "mdl_WW " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_WW << endl;
  cout << setw(20) <<  "mdl_WZ " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_WZ << endl;
  cout << setw(20) <<  "mdl_WT " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_WT << endl;
  cout << setw(20) <<  "mdl_ymtau " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_ymtau << endl;
  cout << setw(20) <<  "mdl_ymt " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_ymt << endl;
  cout << setw(20) <<  "mdl_ymb " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_ymb << endl;
  cout << setw(20) <<  "mdl_ymdo " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_ymdo << endl;
  cout << setw(20) <<  "aS " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << aS << endl;
  cout << setw(20) <<  "mdl_Gf " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_Gf << endl;
  cout << setw(20) <<  "mdl_MH " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_MH << endl;
  cout << setw(20) <<  "mdl_MZ " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_MZ << endl;
  cout << setw(20) <<  "mdl_MTA " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_MTA << endl;
  cout << setw(20) <<  "mdl_Me " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_Me << endl;
  cout << setw(20) <<  "mdl_MT " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_MT << endl;
  cout << setw(20) <<  "mdl_MB " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_MB << endl;
  cout << setw(20) <<  "mdl_MW0 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_MW0 << endl;
  cout << setw(20) <<  "mdl_clequ3Abs " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_clequ3Abs << endl;
  cout << setw(20) <<  "mdl_clequ1Abs " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_clequ1Abs << endl;
  cout << setw(20) <<  "mdl_cquqd8Abs " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_cquqd8Abs << endl;
  cout << setw(20) <<  "mdl_cquqd1Abs " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_cquqd1Abs << endl;
  cout << setw(20) <<  "mdl_cledqAbs " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_cledqAbs << endl;
  cout << setw(20) <<  "mdl_cqd8 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cqd8 << endl;
  cout << setw(20) <<  "mdl_cqd1 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cqd1 << endl;
  cout << setw(20) <<  "mdl_cqu8 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cqu8 << endl;
  cout << setw(20) <<  "mdl_cqu1 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cqu1 << endl;
  cout << setw(20) <<  "mdl_cqe " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cqe << endl;
  cout << setw(20) <<  "mdl_cld " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cld << endl;
  cout << setw(20) <<  "mdl_clu " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_clu << endl;
  cout << setw(20) <<  "mdl_cle " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cle << endl;
  cout << setw(20) <<  "mdl_cud8 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cud8 << endl;
  cout << setw(20) <<  "mdl_cud1 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cud1 << endl;
  cout << setw(20) <<  "mdl_ced " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_ced << endl;
  cout << setw(20) <<  "mdl_ceu " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_ceu << endl;
  cout << setw(20) <<  "mdl_cdd1 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cdd1 << endl;
  cout << setw(20) <<  "mdl_cdd " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cdd << endl;
  cout << setw(20) <<  "mdl_cuu1 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cuu1 << endl;
  cout << setw(20) <<  "mdl_cuu " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cuu << endl;
  cout << setw(20) <<  "mdl_cee " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cee << endl;
  cout << setw(20) <<  "mdl_clq3 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_clq3 << endl;
  cout << setw(20) <<  "mdl_clq1 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_clq1 << endl;
  cout << setw(20) <<  "mdl_cqq31 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cqq31 << endl;
  cout << setw(20) <<  "mdl_cqq3 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cqq3 << endl;
  cout << setw(20) <<  "mdl_cqq11 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cqq11 << endl;
  cout << setw(20) <<  "mdl_cqq1 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cqq1 << endl;
  cout << setw(20) <<  "mdl_cll1 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cll1 << endl;
  cout << setw(20) <<  "mdl_cll " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cll << endl;
  cout << setw(20) <<  "mdl_cHudAbs " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_cHudAbs << endl;
  cout << setw(20) <<  "mdl_cHd " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cHd << endl;
  cout << setw(20) <<  "mdl_cHu " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cHu << endl;
  cout << setw(20) <<  "mdl_cHq3 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cHq3 << endl;
  cout << setw(20) <<  "mdl_cHq1 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cHq1 << endl;
  cout << setw(20) <<  "mdl_cHe " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cHe << endl;
  cout << setw(20) <<  "mdl_cHl3 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cHl3 << endl;
  cout << setw(20) <<  "mdl_cHl1 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cHl1 << endl;
  cout << setw(20) <<  "mdl_cdBAbs " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_cdBAbs << endl;
  cout << setw(20) <<  "mdl_cdWAbs " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_cdWAbs << endl;
  cout << setw(20) <<  "mdl_cdGAbs " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_cdGAbs << endl;
  cout << setw(20) <<  "mdl_cuBAbs " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_cuBAbs << endl;
  cout << setw(20) <<  "mdl_cuWAbs " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_cuWAbs << endl;
  cout << setw(20) <<  "mdl_cuGAbs " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_cuGAbs << endl;
  cout << setw(20) <<  "mdl_ceBAbs " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_ceBAbs << endl;
  cout << setw(20) <<  "mdl_ceWAbs " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_ceWAbs << endl;
  cout << setw(20) <<  "mdl_cdHAbs " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_cdHAbs << endl;
  cout << setw(20) <<  "mdl_cuHAbs " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_cuHAbs << endl;
  cout << setw(20) <<  "mdl_ceHAbs " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_ceHAbs << endl;
  cout << setw(20) <<  "mdl_cHWBtil " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_cHWBtil << endl;
  cout << setw(20) <<  "mdl_cHWB " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cHWB << endl;
  cout << setw(20) <<  "mdl_cHBtil " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_cHBtil << endl;
  cout << setw(20) <<  "mdl_cHB " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cHB << endl;
  cout << setw(20) <<  "mdl_cHWtil " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_cHWtil << endl;
  cout << setw(20) <<  "mdl_cHW " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cHW << endl;
  cout << setw(20) <<  "mdl_cHGtil " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_cHGtil << endl;
  cout << setw(20) <<  "mdl_cHG " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cHG << endl;
  cout << setw(20) <<  "mdl_cHDD " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cHDD << endl;
  cout << setw(20) <<  "mdl_cHbox " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cHbox << endl;
  cout << setw(20) <<  "mdl_cH " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cH << endl;
  cout << setw(20) <<  "mdl_cWtil " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cWtil << endl;
  cout << setw(20) <<  "mdl_cW " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cW << endl;
  cout << setw(20) <<  "mdl_cGtil " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cGtil << endl;
  cout << setw(20) <<  "mdl_cG " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cG << endl;
  cout << setw(20) <<  "mdl_LambdaSMEFT " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_LambdaSMEFT << endl;
  cout << setw(20) <<  "mdl_ceWPh " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_ceWPh << endl;
  cout << setw(20) <<  "mdl_cabi " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cabi << endl;
  cout << setw(20) <<  "mdl_MS " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_MS << endl;
  cout << setw(20) <<  "mdl_MD " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_MD << endl;
  cout << setw(20) <<  "mdl_MC " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_MC << endl;
  cout << setw(20) <<  "mdl_MU " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_MU << endl;
  cout << setw(20) <<  "mdl_MMU " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_MMU << endl;
  cout << setw(20) <<  "mdl_ymm " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_ymm << endl;
  cout << setw(20) <<  "mdl_yme " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_yme << endl;
  cout << setw(20) <<  "mdl_ymc " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_ymc << endl;
  cout << setw(20) <<  "mdl_yms " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_yms << endl;
  cout << setw(20) <<  "mdl_ymup " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_ymup << endl;
  cout << setw(20) <<  "mdl_clequ3Ph " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_clequ3Ph << endl;
  cout << setw(20) <<  "mdl_clequ1Ph " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_clequ1Ph << endl;
  cout << setw(20) <<  "mdl_cquqd8Ph " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_cquqd8Ph << endl;
  cout << setw(20) <<  "mdl_cquqd1Ph " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_cquqd1Ph << endl;
  cout << setw(20) <<  "mdl_cledqPh " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_cledqPh << endl;
  cout << setw(20) <<  "mdl_cdHPh " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cdHPh << endl;
  cout << setw(20) <<  "mdl_cuHPh " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cuHPh << endl;
  cout << setw(20) <<  "mdl_ceHPh " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_ceHPh << endl;
  cout << setw(20) <<  "mdl_cHudPh " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_cHudPh << endl;
  cout << setw(20) <<  "mdl_cdBPh " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cdBPh << endl;
  cout << setw(20) <<  "mdl_cdWPh " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cdWPh << endl;
  cout << setw(20) <<  "mdl_cdGPh " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cdGPh << endl;
  cout << setw(20) <<  "mdl_cuBPh " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cuBPh << endl;
  cout << setw(20) <<  "mdl_cuWPh " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cuWPh << endl;
  cout << setw(20) <<  "mdl_cuGPh " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cuGPh << endl;
  cout << setw(20) <<  "mdl_ceBPh " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_ceBPh << endl;
  cout << setw(20) <<  "mdl_CKMeta " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_CKMeta << endl;
  cout << setw(20) <<  "mdl_CKMrho " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_CKMrho << endl;
  cout << setw(20) <<  "mdl_CKMA " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_CKMA << endl;
  cout << setw(20) <<  "mdl_CKMlambda " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_CKMlambda << endl;
  cout << setw(20) <<  "mdl_conjg__CKM2x2 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_conjg__CKM2x2 << endl;
  cout << setw(20) <<  "mdl_CKM1x1 " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_CKM1x1 << endl;
  cout << setw(20) <<  "mdl_conjg__CKM3x3 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_conjg__CKM3x3 << endl;
  cout << setw(20) <<  "mdl_conjg__CKM1x1 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_conjg__CKM1x1 << endl;
  cout << setw(20) <<  "mdl_CKM2x2 " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_CKM2x2 << endl;
  cout << setw(20) <<  "mdl_CKM3x3 " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_CKM3x3 << endl;
  cout << setw(20) <<  "mdl_complexi " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_complexi << endl;
  cout << setw(20) <<  "mdl_ceH " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_ceH << endl;
  cout << setw(20) <<  "mdl_cuH " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cuH << endl;
  cout << setw(20) <<  "mdl_cdH " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cdH << endl;
  cout << setw(20) <<  "mdl_ceW " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_ceW << endl;
  cout << setw(20) <<  "mdl_ceB " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_ceB << endl;
  cout << setw(20) <<  "mdl_cuG " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cuG << endl;
  cout << setw(20) <<  "mdl_cuW " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cuW << endl;
  cout << setw(20) <<  "mdl_cuB " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cuB << endl;
  cout << setw(20) <<  "mdl_cdG " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cdG << endl;
  cout << setw(20) <<  "mdl_cdW " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cdW << endl;
  cout << setw(20) <<  "mdl_cdB " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cdB << endl;
  cout << setw(20) <<  "mdl_cHud " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cHud << endl;
  cout << setw(20) <<  "mdl_cledq " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cledq << endl;
  cout << setw(20) <<  "mdl_cquqd1 " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_cquqd1 << endl;
  cout << setw(20) <<  "mdl_cquqd8 " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_cquqd8 << endl;
  cout << setw(20) <<  "mdl_clequ1 " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_clequ1 << endl;
  cout << setw(20) <<  "mdl_clequ3 " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_clequ3 << endl;
  cout << setw(20) <<  "mdl_MW " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_MW << endl;
  cout << setw(20) <<  "mdl_nb__2__exp__0_25 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_nb__2__exp__0_25 << endl;
  cout << setw(20) <<  "mdl_MH__exp__2 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_MH__exp__2 << endl;
  cout << setw(20) <<  "mdl_sqrt__2 " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_sqrt__2 << endl;
  cout << setw(20) <<  "mdl_CKMlambda__exp__2 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_CKMlambda__exp__2 << endl;
  cout << setw(20) <<  "mdl_CKM1x2 " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_CKM1x2 << endl;
  cout << setw(20) <<  "mdl_CKMlambda__exp__3 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_CKMlambda__exp__3 << endl;
  cout << setw(20) <<  "mdl_CKM2x1 " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_CKM2x1 << endl;
  cout << setw(20) <<  "mdl_CKM2x3 " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_CKM2x3 << endl;
  cout << setw(20) <<  "mdl_CKM3x2 " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_CKM3x2 << endl;
  cout << setw(20) <<  "mdl_MW__exp__2 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_MW__exp__2 << endl;
  cout << setw(20) <<  "mdl_MZ__exp__2 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_MZ__exp__2 << endl;
  cout << setw(20) <<  "mdl_sth2 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_sth2 << endl;
  cout << setw(20) <<  "mdl_LambdaSMEFT__exp__2 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_LambdaSMEFT__exp__2 <<
      endl;
  cout << setw(20) <<  "mdl_MH__exp__6 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_MH__exp__6 << endl;
  cout << setw(20) <<  "mdl_MT__exp__6 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_MT__exp__6 << endl;
  cout << setw(20) <<  "mdl_MH__exp__4 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_MH__exp__4 << endl;
  cout << setw(20) <<  "mdl_MT__exp__4 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_MT__exp__4 << endl;
  cout << setw(20) <<  "mdl_MT__exp__2 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_MT__exp__2 << endl;
  cout << setw(20) <<  "mdl_cth " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_cth << endl;
  cout << setw(20) <<  "mdl_sqrt__sth2 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_sqrt__sth2 << endl;
  cout << setw(20) <<  "mdl_sth " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_sth << endl;
  cout << setw(20) <<  "mdl_MW0__exp__6 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_MW0__exp__6 << endl;
  cout << setw(20) <<  "mdl_MW0__exp__4 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_MW0__exp__4 << endl;
  cout << setw(20) <<  "mdl_MW0__exp__2 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_MW0__exp__2 << endl;
  cout << setw(20) <<  "mdl_MZ__exp__4 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_MZ__exp__4 << endl;
  cout << setw(20) <<  "mdl_MZ__exp__6 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_MZ__exp__6 << endl;
  cout << setw(20) <<  "mdl_cth__exp__2 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_cth__exp__2 << endl;
  cout << setw(20) <<  "mdl_sth__exp__2 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_sth__exp__2 << endl;
  cout << setw(20) <<  "mdl_conjg__CKM2x1 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_conjg__CKM2x1 << endl;
  cout << setw(20) <<  "mdl_conjg__CKM1x2 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_conjg__CKM1x2 << endl;
  cout << setw(20) <<  "mdl_conjg__CKM3x2 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_conjg__CKM3x2 << endl;
  cout << setw(20) <<  "mdl_conjg__CKM2x3 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_conjg__CKM2x3 << endl;
  cout << setw(20) <<  "mdl_cth__exp__4 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_cth__exp__4 << endl;
  cout << setw(20) <<  "mdl_cth__exp__3 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_cth__exp__3 << endl;
  cout << setw(20) <<  "mdl_sth__exp__3 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_sth__exp__3 << endl;
  cout << setw(20) <<  "mdl_sth__exp__4 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_sth__exp__4 << endl;
  cout << setw(20) <<  "mdl_LambdaSMEFT__exp__4 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_LambdaSMEFT__exp__4 <<
      endl;
  cout << setw(20) <<  "mdl_conjg__cdG " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_conjg__cdG << endl;
  cout << setw(20) <<  "mdl_conjg__cdH " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_conjg__cdH << endl;
  cout << setw(20) <<  "mdl_conjg__cdW " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_conjg__cdW << endl;
  cout << setw(20) <<  "mdl_conjg__cdB " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_conjg__cdB << endl;
  cout << setw(20) <<  "mdl_conjg__ceH " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_conjg__ceH << endl;
  cout << setw(20) <<  "mdl_conjg__ceW " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_conjg__ceW << endl;
  cout << setw(20) <<  "mdl_conjg__ceB " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_conjg__ceB << endl;
  cout << setw(20) <<  "mdl_conjg__cHud " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_conjg__cHud << endl;
  cout << setw(20) <<  "mdl_conjg__cledq " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_conjg__cledq << endl;
  cout << setw(20) <<  "mdl_conjg__clequ1 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_conjg__clequ1 << endl;
  cout << setw(20) <<  "mdl_conjg__clequ3 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_conjg__clequ3 << endl;
  cout << setw(20) <<  "mdl_conjg__cquqd1 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_conjg__cquqd1 << endl;
  cout << setw(20) <<  "mdl_conjg__cquqd8 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_conjg__cquqd8 << endl;
  cout << setw(20) <<  "mdl_conjg__cuG " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_conjg__cuG << endl;
  cout << setw(20) <<  "mdl_conjg__cuH " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_conjg__cuH << endl;
  cout << setw(20) <<  "mdl_conjg__cuW " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_conjg__cuW << endl;
  cout << setw(20) <<  "mdl_conjg__cuB " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_conjg__cuB << endl;
  cout << setw(20) <<  "mdl_sqrt__Gf " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_sqrt__Gf << endl;
  cout << setw(20) <<  "mdl_vevhat " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_vevhat << endl;
  cout << setw(20) <<  "mdl_lam " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_lam << endl;
  cout << setw(20) <<  "mdl_aEW " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_aEW << endl;
  cout << setw(20) <<  "mdl_vevhat__exp__2 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_vevhat__exp__2 << endl;
  cout << setw(20) <<  "mdl_dGf " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_dGf << endl;
  cout << setw(20) <<  "mdl_dMH2 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_dMH2 << endl;
  cout << setw(20) <<  "mdl_yb " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_yb << endl;
  cout << setw(20) <<  "mdl_yc " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_yc << endl;
  cout << setw(20) <<  "mdl_ydo " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_ydo << endl;
  cout << setw(20) <<  "mdl_ye " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_ye << endl;
  cout << setw(20) <<  "mdl_ym " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_ym << endl;
  cout << setw(20) <<  "mdl_ys " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_ys << endl;
  cout << setw(20) <<  "mdl_yt " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_yt << endl;
  cout << setw(20) <<  "mdl_ytau " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_ytau << endl;
  cout << setw(20) <<  "mdl_yup " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_yup << endl;
  cout << setw(20) <<  "mdl_barlam " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_barlam << endl;
  cout << setw(20) <<  "mdl_vevT " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_vevT << endl;
  cout << setw(20) <<  "mdl_dgw " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_dgw << endl;
  cout << setw(20) <<  "mdl_sqrt__aEW " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_sqrt__aEW << endl;
  cout << setw(20) <<  "mdl_ee " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_ee << endl;
  cout << setw(20) <<  "mdl_gwsh " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_gwsh << endl;
  cout << setw(20) <<  "mdl_vev " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_vev << endl;
  cout << setw(20) <<  "mdl_dMZ2 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_dMZ2 << endl;
  cout << setw(20) <<  "mdl_g1 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_g1 << endl;
  cout << setw(20) <<  "mdl_gw " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_gw << endl;
  cout << setw(20) <<  "mdl_ee__exp__2 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_ee__exp__2 << endl;
  cout << setw(20) <<  "mdl_gHaa " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_gHaa << endl;
  cout << setw(20) <<  "mdl_gHza " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_gHza << endl;
  cout << setw(20) <<  "mdl_dg1 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_dg1 << endl;
  cout << setw(20) <<  "mdl_g1sh " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_g1sh << endl;
  cout << setw(20) <<  "mdl_dsth2 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_dsth2 << endl;
  cout << setw(20) <<  "mdl_I1b11 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I1b11 << endl;
  cout << setw(20) <<  "mdl_I1b12 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I1b12 << endl;
  cout << setw(20) <<  "mdl_I1b21 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I1b21 << endl;
  cout << setw(20) <<  "mdl_I1b22 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I1b22 << endl;
  cout << setw(20) <<  "mdl_I1b32 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I1b32 << endl;
  cout << setw(20) <<  "mdl_I1b33 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I1b33 << endl;
  cout << setw(20) <<  "mdl_I2b11 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I2b11 << endl;
  cout << setw(20) <<  "mdl_I2b12 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I2b12 << endl;
  cout << setw(20) <<  "mdl_I2b21 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I2b21 << endl;
  cout << setw(20) <<  "mdl_I2b22 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I2b22 << endl;
  cout << setw(20) <<  "mdl_I2b23 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I2b23 << endl;
  cout << setw(20) <<  "mdl_I2b33 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I2b33 << endl;
  cout << setw(20) <<  "mdl_I3b11 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I3b11 << endl;
  cout << setw(20) <<  "mdl_I3b12 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I3b12 << endl;
  cout << setw(20) <<  "mdl_I3b21 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I3b21 << endl;
  cout << setw(20) <<  "mdl_I3b22 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I3b22 << endl;
  cout << setw(20) <<  "mdl_I3b32 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I3b32 << endl;
  cout << setw(20) <<  "mdl_I3b33 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I3b33 << endl;
  cout << setw(20) <<  "mdl_I4b11 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I4b11 << endl;
  cout << setw(20) <<  "mdl_I4b12 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I4b12 << endl;
  cout << setw(20) <<  "mdl_I4b21 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I4b21 << endl;
  cout << setw(20) <<  "mdl_I4b22 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I4b22 << endl;
  cout << setw(20) <<  "mdl_I4b23 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I4b23 << endl;
  cout << setw(20) <<  "mdl_I4b33 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I4b33 << endl;
  cout << setw(20) <<  "mdl_I5b11 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I5b11 << endl;
  cout << setw(20) <<  "mdl_I5b12 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I5b12 << endl;
  cout << setw(20) <<  "mdl_I5b21 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I5b21 << endl;
  cout << setw(20) <<  "mdl_I5b22 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I5b22 << endl;
  cout << setw(20) <<  "mdl_I5b32 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I5b32 << endl;
  cout << setw(20) <<  "mdl_I5b33 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I5b33 << endl;
  cout << setw(20) <<  "mdl_I6b11 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I6b11 << endl;
  cout << setw(20) <<  "mdl_I6b12 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I6b12 << endl;
  cout << setw(20) <<  "mdl_I6b21 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I6b21 << endl;
  cout << setw(20) <<  "mdl_I6b22 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I6b22 << endl;
  cout << setw(20) <<  "mdl_I6b23 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I6b23 << endl;
  cout << setw(20) <<  "mdl_I6b33 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I6b33 << endl;
  cout << setw(20) <<  "mdl_I7b11 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I7b11 << endl;
  cout << setw(20) <<  "mdl_I7b12 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I7b12 << endl;
  cout << setw(20) <<  "mdl_I7b21 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I7b21 << endl;
  cout << setw(20) <<  "mdl_I7b22 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I7b22 << endl;
  cout << setw(20) <<  "mdl_I7b33 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I7b33 << endl;
  cout << setw(20) <<  "mdl_I8b11 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I8b11 << endl;
  cout << setw(20) <<  "mdl_I8b12 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I8b12 << endl;
  cout << setw(20) <<  "mdl_I8b21 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I8b21 << endl;
  cout << setw(20) <<  "mdl_I8b22 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I8b22 << endl;
  cout << setw(20) <<  "mdl_I8b33 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_I8b33 << endl;
  cout << setw(20) <<  "mdl_vevhat__exp__3 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_vevhat__exp__3 << endl;
  cout << setw(20) <<  "mdl_vevhat__exp__4 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_vevhat__exp__4 << endl;
}
void Parameters_SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless_SMlimit::printIndependentCouplings()
{
  cout <<  "SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless model couplings independent of event kinematics:" <<
      endl;
  cout << setw(20) <<  "GC_3 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << GC_3 << endl;
  cout << setw(20) <<  "GC_234 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << GC_234 << endl;
  cout << setw(20) <<  "GC_249 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << GC_249 << endl;
  cout << setw(20) <<  "GC_313 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << GC_313 << endl;
  cout << setw(20) <<  "GC_315 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << GC_315 << endl;
  cout << setw(20) <<  "GC_319 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << GC_319 << endl;
  cout << setw(20) <<  "GC_320 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << GC_320 << endl;
  cout << setw(20) <<  "GC_321 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << GC_321 << endl;
  cout << setw(20) <<  "GC_322 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << GC_322 << endl;
  cout << setw(20) <<  "GC_324 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << GC_324 << endl;
  cout << setw(20) <<  "GC_325 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << GC_325 << endl;
  cout << setw(20) <<  "GC_328 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << GC_328 << endl;
  cout << setw(20) <<  "GC_337 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << GC_337 << endl;
  cout << setw(20) <<  "GC_344 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << GC_344 << endl;
  cout << setw(20) <<  "GC_349 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << GC_349 << endl;
  cout << setw(20) <<  "GC_358 " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << GC_358 << endl;
}
void Parameters_SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless_SMlimit::printDependentParameters()
{
  cout <<  "SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless_SMlimit model parameters dependent on event kinematics:" <<
      endl;
  cout << setw(20) <<  "mdl_sqrt__aS " <<  "= " << setiosflags(ios::scientific)
      << setw(10) << mdl_sqrt__aS << endl;
  cout << setw(20) <<  "G " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << G << endl;
  cout << setw(20) <<  "mdl_G__exp__2 " <<  "= " <<
      setiosflags(ios::scientific) << setw(10) << mdl_G__exp__2 << endl;
  cout << setw(20) <<  "mdl_gHgg " <<  "= " << setiosflags(ios::scientific) <<
      setw(10) << mdl_gHgg << endl;
}
void Parameters_SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless_SMlimit::printDependentCouplings()
{
  cout <<  "SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless_SMlimit model couplings dependent on event kinematics:" <<
      endl;

}


