//==========================================================================
// This file has been automatically generated for C++
// MadGraph5_aMC@NLO v. 2.6.5, 2018-02-03
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#ifndef Parameters_SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless_SMlimit_H
#define Parameters_SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless_SMlimit_H

#include <complex> 

#include "read_slha.h"
using namespace std; 

class Parameters_SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless_SMlimit
{
  public:

    static Parameters_SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless_SMlimit *
        getInstance();

    // Define "zero"
    double zero, ZERO; 
    // Model parameters independent of aS
    double mdl_WH, mdl_WW, mdl_WZ, mdl_WT, mdl_ymtau, mdl_ymt, mdl_ymb,
        mdl_ymdo, aS, mdl_Gf, mdl_MH, mdl_MZ, mdl_MTA, mdl_Me, mdl_MT, mdl_MB,
        mdl_MW0, mdl_clequ3Abs, mdl_clequ1Abs, mdl_cquqd8Abs, mdl_cquqd1Abs,
        mdl_cledqAbs, mdl_cqd8, mdl_cqd1, mdl_cqu8, mdl_cqu1, mdl_cqe, mdl_cld,
        mdl_clu, mdl_cle, mdl_cud8, mdl_cud1, mdl_ced, mdl_ceu, mdl_cdd1,
        mdl_cdd, mdl_cuu1, mdl_cuu, mdl_cee, mdl_clq3, mdl_clq1, mdl_cqq31,
        mdl_cqq3, mdl_cqq11, mdl_cqq1, mdl_cll1, mdl_cll, mdl_cHudAbs, mdl_cHd,
        mdl_cHu, mdl_cHq3, mdl_cHq1, mdl_cHe, mdl_cHl3, mdl_cHl1, mdl_cdBAbs,
        mdl_cdWAbs, mdl_cdGAbs, mdl_cuBAbs, mdl_cuWAbs, mdl_cuGAbs, mdl_ceBAbs,
        mdl_ceWAbs, mdl_cdHAbs, mdl_cuHAbs, mdl_ceHAbs, mdl_cHWBtil, mdl_cHWB,
        mdl_cHBtil, mdl_cHB, mdl_cHWtil, mdl_cHW, mdl_cHGtil, mdl_cHG,
        mdl_cHDD, mdl_cHbox, mdl_cH, mdl_cWtil, mdl_cW, mdl_cGtil, mdl_cG,
        mdl_LambdaSMEFT, mdl_ceWPh, mdl_cabi, mdl_MS, mdl_MD, mdl_MC, mdl_MU,
        mdl_MMU, mdl_ymm, mdl_yme, mdl_ymc, mdl_yms, mdl_ymup, mdl_clequ3Ph,
        mdl_clequ1Ph, mdl_cquqd8Ph, mdl_cquqd1Ph, mdl_cledqPh, mdl_cdHPh,
        mdl_cuHPh, mdl_ceHPh, mdl_cHudPh, mdl_cdBPh, mdl_cdWPh, mdl_cdGPh,
        mdl_cuBPh, mdl_cuWPh, mdl_cuGPh, mdl_ceBPh, mdl_CKMeta, mdl_CKMrho,
        mdl_CKMA, mdl_CKMlambda, mdl_conjg__CKM2x2, mdl_CKM1x1,
        mdl_conjg__CKM3x3, mdl_conjg__CKM1x1, mdl_CKM2x2, mdl_CKM3x3, mdl_MW,
        mdl_nb__2__exp__0_25, mdl_MH__exp__2, mdl_sqrt__2,
        mdl_CKMlambda__exp__2, mdl_CKMlambda__exp__3, mdl_MW__exp__2,
        mdl_MZ__exp__2, mdl_sth2, mdl_LambdaSMEFT__exp__2, mdl_MH__exp__6,
        mdl_MT__exp__6, mdl_MH__exp__4, mdl_MT__exp__4, mdl_MT__exp__2,
        mdl_cth, mdl_sqrt__sth2, mdl_sth, mdl_MW0__exp__6, mdl_MW0__exp__4,
        mdl_MW0__exp__2, mdl_MZ__exp__4, mdl_MZ__exp__6, mdl_cth__exp__2,
        mdl_sth__exp__2, mdl_cth__exp__4, mdl_cth__exp__3, mdl_sth__exp__3,
        mdl_sth__exp__4, mdl_LambdaSMEFT__exp__4, mdl_sqrt__Gf, mdl_vevhat,
        mdl_lam, mdl_aEW, mdl_vevhat__exp__2, mdl_dGf, mdl_dMH2, mdl_yb,
        mdl_yc, mdl_ydo, mdl_ye, mdl_ym, mdl_ys, mdl_yt, mdl_ytau, mdl_yup,
        mdl_barlam, mdl_vevT, mdl_dgw, mdl_sqrt__aEW, mdl_ee, mdl_gwsh,
        mdl_vev, mdl_dMZ2, mdl_g1, mdl_gw, mdl_ee__exp__2, mdl_gHaa, mdl_gHza,
        mdl_dg1, mdl_g1sh, mdl_dsth2, mdl_vevhat__exp__3, mdl_vevhat__exp__4;
    std::complex<double> mdl_complexi, mdl_ceH, mdl_cuH, mdl_cdH, mdl_ceW,
        mdl_ceB, mdl_cuG, mdl_cuW, mdl_cuB, mdl_cdG, mdl_cdW, mdl_cdB,
        mdl_cHud, mdl_cledq, mdl_cquqd1, mdl_cquqd8, mdl_clequ1, mdl_clequ3,
        mdl_CKM1x2, mdl_CKM2x1, mdl_CKM2x3, mdl_CKM3x2, mdl_conjg__CKM2x1,
        mdl_conjg__CKM1x2, mdl_conjg__CKM3x2, mdl_conjg__CKM2x3,
        mdl_conjg__cdG, mdl_conjg__cdH, mdl_conjg__cdW, mdl_conjg__cdB,
        mdl_conjg__ceH, mdl_conjg__ceW, mdl_conjg__ceB, mdl_conjg__cHud,
        mdl_conjg__cledq, mdl_conjg__clequ1, mdl_conjg__clequ3,
        mdl_conjg__cquqd1, mdl_conjg__cquqd8, mdl_conjg__cuG, mdl_conjg__cuH,
        mdl_conjg__cuW, mdl_conjg__cuB, mdl_I1b11, mdl_I1b12, mdl_I1b21,
        mdl_I1b22, mdl_I1b32, mdl_I1b33, mdl_I2b11, mdl_I2b12, mdl_I2b21,
        mdl_I2b22, mdl_I2b23, mdl_I2b33, mdl_I3b11, mdl_I3b12, mdl_I3b21,
        mdl_I3b22, mdl_I3b32, mdl_I3b33, mdl_I4b11, mdl_I4b12, mdl_I4b21,
        mdl_I4b22, mdl_I4b23, mdl_I4b33, mdl_I5b11, mdl_I5b12, mdl_I5b21,
        mdl_I5b22, mdl_I5b32, mdl_I5b33, mdl_I6b11, mdl_I6b12, mdl_I6b21,
        mdl_I6b22, mdl_I6b23, mdl_I6b33, mdl_I7b11, mdl_I7b12, mdl_I7b21,
        mdl_I7b22, mdl_I7b33, mdl_I8b11, mdl_I8b12, mdl_I8b21, mdl_I8b22,
        mdl_I8b33;
    // Model parameters dependent on aS
    double mdl_sqrt__aS, G, mdl_G__exp__2, mdl_gHgg; 
    // Model couplings independent of aS
    std::complex<double> GC_3, GC_234, GC_249, GC_313, GC_315, GC_319, GC_320,
        GC_321, GC_322, GC_324, GC_325, GC_328, GC_337, GC_344, GC_349, GC_358;
    // Model couplings dependent on aS


    // Set parameters that are unchanged during the run
    void setIndependentParameters(SLHAReader& slha); 
    // Set couplings that are unchanged during the run
    void setIndependentCouplings(); 
    // Set parameters that are changed event by event
    void setDependentParameters(); 
    // Set couplings that are changed event by event
    void setDependentCouplings(); 

    // Print parameters that are unchanged during the run
    void printIndependentParameters(); 
    // Print couplings that are unchanged during the run
    void printIndependentCouplings(); 
    // Print parameters that are changed event by event
    void printDependentParameters(); 
    // Print couplings that are changed event by event
    void printDependentCouplings(); 


  private:
    static Parameters_SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless_SMlimit * instance; 
}; 

#endif  // Parameters_SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless_SMlimit_H

