#include <iostream>

#include "MLM_SM.h"
#include "MLM_EFT0.h"
#include "MLM_EFT_H4l.h"
#include "MLM_EFT_ggF_H4l.h"
#include "MLM_qqZZ.h"
#include "H4lObservables.h"

#include "TFile.h"
#include "TTree.h"
#include "TLorentzVector.h"
#include "TTreeReader.h"
#include "TTreeReaderValue.h"
#include "TTreeReaderArray.h"

#include "TGenPhaseSpace.h"

int main(int argc, char** argv){

  // At some point add option to read command line arguments...
  TString  input_file = "events.root"; // LHE events
  TString output_file = "mlm_1M.root"; // LHE events
  int mode = 0; // 0 = all, 1 = same flavor, 2 = opposite flavor pairs
  int maxEvents = 1000000;
  int autoSave = 10000;
  
  cout << "Opening input " << input_file << endl;
  TFile* input = TFile::Open(input_file); // LHE events
  TTreeReader reader("LHEF", input);
  
  TTreeReaderArray<Double_t> weight(reader, "Event.Weight");
  TTreeReaderArray<Double_t> px(reader, "Particle.Px");
  TTreeReaderArray<Double_t> py(reader, "Particle.Py");
  TTreeReaderArray<Double_t> pz(reader, "Particle.Pz");
  TTreeReaderArray<Double_t> pm(reader, "Particle.M");
  TTreeReaderArray<Int_t>   pid(reader, "Particle.PID");
  TTreeReaderArray<Int_t>  stat(reader, "Particle.Status");
  
  //std::vector<TString> eftPars = { "cG", "cHB", "cHW", "cHWB" };
  std::vector<TString> eftPars = { "SM", "cHB", "cHW", "cHG", "cHGtil", "cHWB", "cHBtil", "cHWtil", "cHWBtil", "cHe", "cHl1", "cHl3", "cHDD", "cHbox", "cH" };
  
  cout << "Setting up output " << output_file << endl;
  double mlm_SM, mlm_qqZZ, mlm_EFT0, m12, m13, m14, m23, m24, m34, cthstr, phi1, cth1, cth2, phi;
  double mlm_EFT_H4l[99];
  TLorentzVector pH, p1, p2, p3, p4, pq0, pq1;
  int pid1, pid2, pid3, pid4;
  TFile* output = TFile::Open(output_file, "RECREATE"); // output
  TTree* tree = new TTree("mlmTree", "");
  tree->SetAutoSave(autoSave);
  tree->Branch("mlm_SM"  , &mlm_SM);
  tree->Branch("mlm_qqZZ"  , &mlm_qqZZ);
  tree->Branch("mlm_EFT0", &mlm_EFT0);
  tree->Branch("m12", &m12);
  tree->Branch("m13", &m13);
  tree->Branch("m14", &m14);
  tree->Branch("m23", &m23);
  tree->Branch("m24", &m24);
  tree->Branch("m34", &m34);
  tree->Branch("cthstr", &cthstr);
  tree->Branch("phi1",   &phi1);
  tree->Branch("phi",    &phi);
  tree->Branch("cth1",   &cth1);
  tree->Branch("cth2",   &cth2);
  tree->Branch("pH",     &pH);
  tree->Branch("p1",     &p1);
  tree->Branch("p2",     &p2);
  tree->Branch("p3",     &p3);
  tree->Branch("p4",     &p4);
  tree->Branch("pid1",   &pid1);
  tree->Branch("pid2",   &pid2);
  tree->Branch("pid3",   &pid3);
  tree->Branch("pid4",   &pid4);
  tree->Branch("pq0",    &pq0);
  tree->Branch("pq1",    &pq1);
  for (unsigned int i = 0; i < eftPars.size(); i++) { tree->Branch("mlm_EFT_" + eftPars[i], &mlm_EFT_H4l[i]); }

  cout << "Initializing MLM interface" << endl;
  // Create an MLM evaluator object
  MLM_SM   mlmCalc_SM;
  MLM_qqZZ mlmCalc_qqZZ;
  MLM_EFT0 mlmCalc_EFT0;
  MLM_EFT_H4l*  mlmCalc_EFT_H4l[99];
  for (unsigned int i = 0; i < eftPars.size(); i++) mlmCalc_EFT_H4l[i] = new MLM_EFT_H4l("Cards/SMEFTsim_massless_" + eftPars[i] + ".dat");
  
  cout << "Processing events" << endl;
  int n = 0;
  while (reader.Next()) {
    n++;
    if (maxEvents > 0 && n > maxEvents) break;
    unsigned int gotWhat = 0;
    std::vector<TLorentzVector> p(5);
    std::vector<int> pdg(5);
    std::vector<unsigned int> first_pair_indices(2);
    std::vector<TLorentzVector> pPair(2);
    for (unsigned int k = 0; k < 2; k++) { // which pair we are looking for (k==0 : first pair, k==1 : second pair)
      for (unsigned int i = 0; i < px.GetSize(); i++) {
        TLorentzVector pi; pi.SetXYZM(px[i], py[i], pz[i], pm[i]);
        //cout << stat[i] << " " << pid[i] << endl;
        if (k == 0 && stat[i] == -1 && pid[i] == 25) {  gotWhat |= 1; p[0] = pi; continue; } // get the Higgs (first pair only)
        if (k == 1 && (i == first_pair_indices[0] || i == first_pair_indices[1])) continue; // we don't want overlaps with the first pair
        if (stat[i] != 1 || (abs(pid[i]) != 11 && abs(pid[i]) != 13)) continue; // Otherwise loop over leptons only
        for (unsigned int j = 0; j < i; j++) {
          if (k == 1 && (j == first_pair_indices[0] || j == first_pair_indices[1])) continue; // we don't want overlaps with the first pair
          if (pid[j] != -pid[i]) continue; // loop over leptons that have the same flavor and opposite sign as the current one only
          if (k == 0) gotWhat |= 2; else gotWhat |= 4;
          TLorentzVector pj; pj.SetXYZM(px[j], py[j], pz[j], pm[j]);
          TLorentzVector p2 = pi + pj;
          if (p2.M() > pPair[k].M()) { // We have a new highest-M pair: store it
            pPair[k] = p2;
            p[1 + 2*k] = pi;
            p[2 + 2*k] = pj;
            pdg[1 + 2*k] = pid[i];
            pdg[2 + 2*k] = pid[j];
            if (k == 0) {
              first_pair_indices[0] = i;
              first_pair_indices[1] = j;
            }
          }
        }
      }
    }
    
    if (gotWhat < 7) {
      cout << "Did not find all the particles (got " << gotWhat << "), skipping entry" << endl;
      continue;
    }
    
    if (pdg[1] > 0) {  // put the positive-charged lepton in first position
      std::swap(p[1], p[2]);
      std::swap(pdg[1], pdg[2]);
    }
    if (pdg[3] > 0) {  // put the positive-charged lepton in first position
      std::swap(p[3], p[4]);
      std::swap(pdg[3], pdg[4]);
    }
    
    if (mode == 1 && pdg[1] != pdg[3]) continue;
    if (mode == 2 && pdg[1] == pdg[3]) continue;
    
    m12 = (p[1] + p[2]).M();
    m13 = (p[1] + p[3]).M();
    m14 = (p[1] + p[4]).M();
    m23 = (p[2] + p[3]).M();
    m24 = (p[2] + p[4]).M();
    m34 = (p[3] + p[4]).M();

    std::vector<TLorentzVector> p_qqZZ(6);
    TGenPhaseSpace ps;
    Double_t decaymass[2]={0,0};
    if(!ps.SetDecay(p[0],2,decaymass)) {
      cout<<"Generating incoming quarks for qqZZ failed!"<<endl;
    } else {
      ps.Generate();
      p_qqZZ[0]=*ps.GetDecay(0);
      p_qqZZ[1]=*ps.GetDecay(1);
    }
    p_qqZZ[2]=p[1];
    p_qqZZ[3]=p[2];
    p_qqZZ[4]=p[3];
    p_qqZZ[5]=p[4];
    
    //cout<<"p(Higgs): x="<<p[0].X()<<" y="<<p[0].Y()<<" z="<<p[0].Z()<<" E="<<p[0].E()<<endl; 
    //cout<<"p(q0): x="<<p_qqZZ[0].X()<<" y="<<p_qqZZ[0].Y()<<" z="<<p_qqZZ[0].Z()<<" E="<<p_qqZZ[0].E()<<endl; 
    //cout<<"p(q1): x="<<p_qqZZ[1].X()<<" y="<<p_qqZZ[1].Y()<<" z="<<p_qqZZ[1].Z()<<" E="<<p_qqZZ[1].E()<<endl; 
    
    H4lObservables::helicityAngles(p[2], p[1], p[4], p[3], cthstr, phi1, phi, cth1, cth2);
    mlm_SM   = mlmCalc_SM.Eval_eeee(p);
    mlm_qqZZ = mlmCalc_qqZZ.Eval_uu(p_qqZZ); //Missing evaluation of dd and antiparticles! Needs PDF weighting
    mlm_EFT0 = mlmCalc_EFT0.Eval_eeee(p);
    for (unsigned int i = 0; i < eftPars.size(); i++) mlm_EFT_H4l[i]  = mlmCalc_EFT_H4l[i]->Eval_eeee(p);
    if (n < 100) {
      cout << "mlm = " << mlm_SM << " " << mlm_EFT0 << " " << mlm_EFT_H4l[0] << endl;
      cout << "mlm_qqZZ = " << mlm_qqZZ << endl;
    } else if (n % autoSave == 0) {
      cout<<"#event="<<n<<endl;
    }
    pH = p[0];
    p1 = p[1];
    p2 = p[2];
    p3 = p[3];
    p4 = p[4];
    pid1 = pdg[1];
    pid2 = pdg[2];
    pid3 = pdg[3];
    pid4 = pdg[4];
    pq0= p_qqZZ[0];
    pq1= p_qqZZ[1];
    tree->Fill();
  }
  output->cd();
  tree->Write();
  delete output;
  
  return 0;
}
