//==========================================================================
// This file has been automatically generated for C++ Standalone by
// MadGraph5_aMC@NLO v. 2.6.5, 2018-02-03
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "CPPProcess.h"
#include "HelAmps_SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless_ggF_H4l.h"

using namespace MG5_SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless_ggF_H4l; 
namespace MG5_Sigma_SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless_gg_epemmupmum_ggF_H4l {

//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: g g > h WEIGHTED<=3 @1
// *   Decay: h > e+ e- mu+ mu- NP<=1 QED<=3

//--------------------------------------------------------------------------
// Initialize process.

void CPPProcess::initProc(string param_card_name) 
{
  // Instantiate the model class and set parameters that stay fixed during run
  pars = new Parameters_SMEFTsim_A_U35_MwScheme_UFO_v2_1_massless_ggF_H4l(); 
  SLHAReader slha(param_card_name); 
  pars->setIndependentParameters(slha); 
  pars->setIndependentCouplings(); 
  pars->printIndependentParameters(); 
  pars->printIndependentCouplings(); 
  // Set external particle masses for this matrix element
  mME.push_back(pars->ZERO); 
  mME.push_back(pars->ZERO); 
  mME.push_back(pars->mdl_Me); 
  mME.push_back(pars->mdl_Me); 
  mME.push_back(pars->mdl_Me); 
  mME.push_back(pars->mdl_Me); 
  jamp2[0] = new double[1]; 
}

//--------------------------------------------------------------------------
// Evaluate |M|^2, part independent of incoming flavour.

void CPPProcess::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  static bool firsttime = true; 
  if (firsttime)
  {
    pars->printDependentParameters(); 
    pars->printDependentCouplings(); 
    firsttime = false; 
  }

  // Reset color flows
  for(int i = 0; i < 1; i++ )
    jamp2[0][i] = 0.; 

  // Local variables and constants
  const int ncomb = 64; 
  static bool goodhel[ncomb] = {ncomb * false}; 
  static int ntry = 0, sum_hel = 0, ngood = 0; 
  static int igood[ncomb]; 
  static int jhel; 
  std::complex<double> * * wfs; 
  double t[nprocesses]; 
  // Helicities for the process
  static const int helicities[ncomb][nexternal] = {{-1, -1, -1, -1, -1, -1},
      {-1, -1, -1, -1, -1, 1}, {-1, -1, -1, -1, 1, -1}, {-1, -1, -1, -1, 1, 1},
      {-1, -1, -1, 1, -1, -1}, {-1, -1, -1, 1, -1, 1}, {-1, -1, -1, 1, 1, -1},
      {-1, -1, -1, 1, 1, 1}, {-1, -1, 1, -1, -1, -1}, {-1, -1, 1, -1, -1, 1},
      {-1, -1, 1, -1, 1, -1}, {-1, -1, 1, -1, 1, 1}, {-1, -1, 1, 1, -1, -1},
      {-1, -1, 1, 1, -1, 1}, {-1, -1, 1, 1, 1, -1}, {-1, -1, 1, 1, 1, 1}, {-1,
      1, -1, -1, -1, -1}, {-1, 1, -1, -1, -1, 1}, {-1, 1, -1, -1, 1, -1}, {-1,
      1, -1, -1, 1, 1}, {-1, 1, -1, 1, -1, -1}, {-1, 1, -1, 1, -1, 1}, {-1, 1,
      -1, 1, 1, -1}, {-1, 1, -1, 1, 1, 1}, {-1, 1, 1, -1, -1, -1}, {-1, 1, 1,
      -1, -1, 1}, {-1, 1, 1, -1, 1, -1}, {-1, 1, 1, -1, 1, 1}, {-1, 1, 1, 1,
      -1, -1}, {-1, 1, 1, 1, -1, 1}, {-1, 1, 1, 1, 1, -1}, {-1, 1, 1, 1, 1, 1},
      {1, -1, -1, -1, -1, -1}, {1, -1, -1, -1, -1, 1}, {1, -1, -1, -1, 1, -1},
      {1, -1, -1, -1, 1, 1}, {1, -1, -1, 1, -1, -1}, {1, -1, -1, 1, -1, 1}, {1,
      -1, -1, 1, 1, -1}, {1, -1, -1, 1, 1, 1}, {1, -1, 1, -1, -1, -1}, {1, -1,
      1, -1, -1, 1}, {1, -1, 1, -1, 1, -1}, {1, -1, 1, -1, 1, 1}, {1, -1, 1, 1,
      -1, -1}, {1, -1, 1, 1, -1, 1}, {1, -1, 1, 1, 1, -1}, {1, -1, 1, 1, 1, 1},
      {1, 1, -1, -1, -1, -1}, {1, 1, -1, -1, -1, 1}, {1, 1, -1, -1, 1, -1}, {1,
      1, -1, -1, 1, 1}, {1, 1, -1, 1, -1, -1}, {1, 1, -1, 1, -1, 1}, {1, 1, -1,
      1, 1, -1}, {1, 1, -1, 1, 1, 1}, {1, 1, 1, -1, -1, -1}, {1, 1, 1, -1, -1,
      1}, {1, 1, 1, -1, 1, -1}, {1, 1, 1, -1, 1, 1}, {1, 1, 1, 1, -1, -1}, {1,
      1, 1, 1, -1, 1}, {1, 1, 1, 1, 1, -1}, {1, 1, 1, 1, 1, 1}};
  // Denominators: spins, colors and identical particles
  const int denominators[nprocesses] = {256}; 

  ntry = ntry + 1; 

  // Reset the matrix elements
  for(int i = 0; i < nprocesses; i++ )
  {
    matrix_element[i] = 0.; 
  }
  // Define permutation
  int perm[nexternal]; 
  for(int i = 0; i < nexternal; i++ )
  {
    perm[i] = i; 
  }

  if (sum_hel == 0 || ntry < 10)
  {
    // Calculate the matrix element for all helicities
    for(int ihel = 0; ihel < ncomb; ihel++ )
    {
      if (goodhel[ihel] || ntry < 2)
      {
        calculate_wavefunctions(perm, helicities[ihel]); 
        t[0] = matrix_1_gg_h_h_epemmupmum(); 

        double tsum = 0; 
        for(int iproc = 0; iproc < nprocesses; iproc++ )
        {
          matrix_element[iproc] += t[iproc]; 
          tsum += t[iproc]; 
        }
        // Store which helicities give non-zero result
        if (tsum != 0. && !goodhel[ihel])
        {
          goodhel[ihel] = true; 
          ngood++; 
          igood[ngood] = ihel; 
        }
      }
    }
    jhel = 0; 
    sum_hel = min(sum_hel, ngood); 
  }
  else
  {
    // Only use the "good" helicities
    for(int j = 0; j < sum_hel; j++ )
    {
      jhel++; 
      if (jhel >= ngood)
        jhel = 0; 
      double hwgt = double(ngood)/double(sum_hel); 
      int ihel = igood[jhel]; 
      calculate_wavefunctions(perm, helicities[ihel]); 
      t[0] = matrix_1_gg_h_h_epemmupmum(); 

      for(int iproc = 0; iproc < nprocesses; iproc++ )
      {
        matrix_element[iproc] += t[iproc] * hwgt; 
      }
    }
  }

  for (int i = 0; i < nprocesses; i++ )
    matrix_element[i] /= denominators[i]; 



}

//--------------------------------------------------------------------------
// Evaluate |M|^2, including incoming flavour dependence.

double CPPProcess::sigmaHat() 
{
  // Select between the different processes
  if(id1 == 21 && id2 == 21)
  {
    // Add matrix elements for processes with beams (21, 21)
    return matrix_element[0]; 
  }
  else
  {
    // Return 0 if not correct initial state assignment
    return 0.; 
  }
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void CPPProcess::calculate_wavefunctions(const int perm[], const int hel[])
{
  // Calculate wavefunctions for all processes
  int i, j; 

  // Calculate all wavefunctions
  vxxxxx(p[perm[0]], mME[0], hel[0], -1, w[0]); 
  vxxxxx(p[perm[1]], mME[1], hel[1], -1, w[1]); 
  ixxxxx(p[perm[2]], mME[2], hel[2], -1, w[2]); 
  oxxxxx(p[perm[3]], mME[3], hel[3], +1, w[3]); 
  ixxxxx(p[perm[4]], mME[4], hel[4], -1, w[4]); 
  oxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  FFV1_6_3(w[2], w[3], -pars->GC_234, pars->GC_249, pars->mdl_MZ, pars->mdl_WZ,
      w[6]);
  FFVS1_3_4(w[4], w[5], w[6], pars->GC_315, pars->GC_313, pars->mdl_MH,
      pars->mdl_WH, w[7]);
  FFV5P0_3(w[2], w[3], -pars->GC_3, pars->ZERO, pars->ZERO, w[8]); 
  FFV5P0_3(w[4], w[5], -pars->GC_3, pars->ZERO, pars->ZERO, w[9]); 
  VVS2_3(w[8], w[9], pars->GC_325, pars->mdl_MH, pars->mdl_WH, w[10]); 
  VVS4_3(w[8], w[9], pars->GC_322, pars->mdl_MH, pars->mdl_WH, w[11]); 
  FFV1_6_3(w[4], w[5], -pars->GC_234, pars->GC_249, pars->mdl_MZ, pars->mdl_WZ,
      w[12]);
  VVS1_3(w[8], w[12], pars->GC_324, pars->mdl_MH, pars->mdl_WH, w[13]); 
  VVS4_3(w[8], w[12], pars->GC_324, pars->mdl_MH, pars->mdl_WH, w[14]); 
  VVS1_3(w[9], w[6], pars->GC_324, pars->mdl_MH, pars->mdl_WH, w[15]); 
  VVS4_3(w[9], w[6], pars->GC_324, pars->mdl_MH, pars->mdl_WH, w[16]); 
  VVS3_3(w[6], w[12], pars->GC_319, pars->mdl_MH, pars->mdl_WH, w[17]); 
  VVS2_4_3(w[6], w[12], pars->GC_321, pars->GC_320, pars->mdl_MH, pars->mdl_WH,
      w[18]);
  VVS3_3(w[6], w[12], pars->GC_358, pars->mdl_MH, pars->mdl_WH, w[19]); 
  FFV3_5_3(w[4], w[5], pars->GC_337, pars->GC_328, pars->mdl_MZ, pars->mdl_WZ,
      w[20]);
  VVS3_3(w[6], w[20], pars->GC_319, pars->mdl_MH, pars->mdl_WH, w[21]); 
  FFV1_6_3(w[4], w[5], pars->GC_344, pars->GC_349, pars->mdl_MZ, pars->mdl_WZ,
      w[22]);
  VVS3_3(w[6], w[22], pars->GC_319, pars->mdl_MH, pars->mdl_WH, w[23]); 
  FFV3_5_3(w[2], w[3], pars->GC_337, pars->GC_328, pars->mdl_MZ, pars->mdl_WZ,
      w[24]);
  VVS3_3(w[24], w[12], pars->GC_319, pars->mdl_MH, pars->mdl_WH, w[25]); 
  FFV1_6_3(w[2], w[3], pars->GC_344, pars->GC_349, pars->mdl_MZ, pars->mdl_WZ,
      w[26]);
  VVS3_3(w[26], w[12], pars->GC_319, pars->mdl_MH, pars->mdl_WH, w[27]); 
  FFVS1_3_4(w[2], w[3], w[12], pars->GC_315, pars->GC_313, pars->mdl_MH,
      pars->mdl_WH, w[28]);

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  VVS2_0(w[0], w[1], w[7], pars->GC_269, amp[0]); 
  VVS2_0(w[0], w[1], w[10], pars->GC_269, amp[1]); 
  VVS2_0(w[0], w[1], w[11], pars->GC_269, amp[2]); 
  VVS2_0(w[0], w[1], w[13], pars->GC_269, amp[3]); 
  VVS2_0(w[0], w[1], w[14], pars->GC_269, amp[4]); 
  VVS2_0(w[0], w[1], w[15], pars->GC_269, amp[5]); 
  VVS2_0(w[0], w[1], w[16], pars->GC_269, amp[6]); 
  VVS2_0(w[0], w[1], w[17], pars->GC_269, amp[7]); 
  VVS2_0(w[0], w[1], w[18], pars->GC_269, amp[8]); 
  VVS2_0(w[0], w[1], w[19], pars->GC_269, amp[9]); 
  VVS2_0(w[0], w[1], w[21], pars->GC_269, amp[10]); 
  VVS2_0(w[0], w[1], w[23], pars->GC_269, amp[11]); 
  VVS2_0(w[0], w[1], w[25], pars->GC_269, amp[12]); 
  VVS2_0(w[0], w[1], w[27], pars->GC_269, amp[13]); 
  VVS2_0(w[0], w[1], w[28], pars->GC_269, amp[14]); 
  VVS4_0(w[0], w[1], w[7], pars->GC_270, amp[15]); 
  VVS4_0(w[0], w[1], w[10], pars->GC_270, amp[16]); 
  VVS4_0(w[0], w[1], w[11], pars->GC_270, amp[17]); 
  VVS4_0(w[0], w[1], w[13], pars->GC_270, amp[18]); 
  VVS4_0(w[0], w[1], w[14], pars->GC_270, amp[19]); 
  VVS4_0(w[0], w[1], w[15], pars->GC_270, amp[20]); 
  VVS4_0(w[0], w[1], w[16], pars->GC_270, amp[21]); 
  VVS4_0(w[0], w[1], w[17], pars->GC_270, amp[22]); 
  VVS4_0(w[0], w[1], w[18], pars->GC_270, amp[23]); 
  VVS4_0(w[0], w[1], w[19], pars->GC_270, amp[24]); 
  VVS4_0(w[0], w[1], w[21], pars->GC_270, amp[25]); 
  VVS4_0(w[0], w[1], w[23], pars->GC_270, amp[26]); 
  VVS4_0(w[0], w[1], w[25], pars->GC_270, amp[27]); 
  VVS4_0(w[0], w[1], w[27], pars->GC_270, amp[28]); 
  VVS4_0(w[0], w[1], w[28], pars->GC_270, amp[29]); 

}
double CPPProcess::matrix_1_gg_h_h_epemmupmum() 
{
  int i, j; 
  // Local variables
  const int ngraphs = 30; 
  const int ncolor = 1; 
  std::complex<double> ztemp; 
  std::complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1}; 
  static const double cf[ncolor][ncolor] = {{2}}; 

  // Calculate color flows
  jamp[0] = +2. * (+amp[0] + amp[1] + amp[2] + amp[3] + amp[4] + amp[5] +
      amp[6] + amp[7] + amp[8] + amp[9] + amp[10] + amp[11] + amp[12] + amp[13]
      + amp[14] + amp[15] + amp[16] + amp[17] + amp[18] + amp[19] + amp[20] +
      amp[21] + amp[22] + amp[23] + amp[24] + amp[25] + amp[26] + amp[27] +
      amp[28] + amp[29]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])); 

  return matrix; 
}

}
