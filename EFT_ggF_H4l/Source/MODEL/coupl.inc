ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      written by the UFO converter
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      DOUBLE PRECISION G
      COMMON/STRONG/ G

      DOUBLE COMPLEX GAL(2)
      COMMON/WEAK/ GAL

      DOUBLE PRECISION MU_R
      COMMON/RSCALE/ MU_R

      DOUBLE PRECISION NF
      PARAMETER(NF=0)

      DOUBLE PRECISION MDL_MT,MDL_MW,MDL_MZ,MDL_MTA,MDL_MB,MDL_ME
     $ ,MDL_MH

      COMMON/MASSES/ MDL_MT,MDL_MW,MDL_MZ,MDL_MTA,MDL_MB,MDL_ME,MDL_MH


      DOUBLE PRECISION MDL_WZ,MDL_WH,MDL_WW,MDL_WT

      COMMON/WIDTHS/ MDL_WZ,MDL_WH,MDL_WW,MDL_WT


      DOUBLE COMPLEX GC_3, GC_234, GC_249, GC_269, GC_270, GC_313,
     $  GC_315, GC_319, GC_320, GC_321, GC_322, GC_324, GC_325, GC_328
     $ , GC_337, GC_344, GC_349, GC_358

      COMMON/COUPLINGS/ GC_3, GC_234, GC_249, GC_269, GC_270, GC_313,
     $  GC_315, GC_319, GC_320, GC_321, GC_322, GC_324, GC_325, GC_328
     $ , GC_337, GC_344, GC_349, GC_358

