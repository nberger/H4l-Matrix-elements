C     This File is Automatically generated by ALOHA 
C     The process calculated in this file is: 
C     Gamma(3,2,-1)*ProjM(-1,1)
C     
      SUBROUTINE FFVS1_4(F1, F2, V3, COUP, M4, W4,S4)
      IMPLICIT NONE
      COMPLEX*16 CI
      PARAMETER (CI=(0D0,1D0))
      COMPLEX*16 DENOM
      COMPLEX*16 V3(*)
      COMPLEX*16 S4(3)
      COMPLEX*16 F1(*)
      COMPLEX*16 F2(*)
      REAL*8 M4
      REAL*8 P4(0:3)
      COMPLEX*16 COUP
      COMPLEX*16 TMP3
      REAL*8 W4
      S4(1) = +F1(1)+F2(1)+V3(1)
      S4(2) = +F1(2)+F2(2)+V3(2)
      P4(0) = -DBLE(S4(1))
      P4(1) = -DBLE(S4(2))
      P4(2) = -DIMAG(S4(2))
      P4(3) = -DIMAG(S4(1))
      TMP3 = (F1(3)*(F2(5)*(V3(3)+V3(6))+F2(6)*(V3(4)+CI*(V3(5))))
     $ +F1(4)*(F2(5)*(V3(4)-CI*(V3(5)))+F2(6)*(V3(3)-V3(6))))
      DENOM = COUP/(P4(0)**2-P4(1)**2-P4(2)**2-P4(3)**2 - M4 * (M4 -CI
     $ * W4))
      S4(3)= DENOM*CI * TMP3
      END


C     This File is Automatically generated by ALOHA 
C     The process calculated in this file is: 
C     Gamma(3,2,-1)*ProjM(-1,1)
C     
      SUBROUTINE FFVS1_3_4(F1, F2, V3, COUP1, COUP2, M4, W4,S4)
      IMPLICIT NONE
      COMPLEX*16 CI
      PARAMETER (CI=(0D0,1D0))
      COMPLEX*16 DENOM
      COMPLEX*16 V3(*)
      COMPLEX*16 S4(3)
      COMPLEX*16 COUP2
      COMPLEX*16 STMP(3)
      COMPLEX*16 F1(*)
      COMPLEX*16 COUP1
      COMPLEX*16 F2(*)
      INTEGER*4 I
      REAL*8 M4
      REAL*8 P4(0:3)
      REAL*8 W4
      CALL FFVS1_4(F1,F2,V3,COUP1,M4,W4,S4)
      CALL FFVS3_4(F1,F2,V3,COUP2,M4,W4,STMP)
      DO I = 3, 3
        S4(I) = S4(I) + STMP(I)
      ENDDO
      END


