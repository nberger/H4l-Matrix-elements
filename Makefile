LIBS=Base SM EFT0 EFT_H4l EFT_ggF_H4l qqZZ

CC=g++
INCDIR=src
LIBDIR=lib
LLIBS=$(foreach L,$(LIBS),-L$L/lib -l$L)
LIBFILES=$(foreach L,$(LIBS),$L/lib/lib$L.so)

CFLAGS=-g -Wall -I$(INCDIR) $(shell root-config --cflags) $(foreach L,$(LIBS),-I$L/src)
LDFLAGS= $(shell root-config --glibs)
EXECUTABLE=MLMExec

all: $(LIBS) $(EXECUTABLE)

# Make the executable
$(EXECUTABLE): src/$(EXECUTABLE).cc $(LIBFILES)
	$(CC) $(CFLAGS) $<  $(LIBFILES) $(LDFLAGS)  -o $@

# Make the libraries: compiles and link all the .cc in LIB/src/
$(foreach L,$(LIBS),$(eval $L/lib/lib$L.so: $(subst src,lib,$(patsubst %.cc,%.o,$(wildcard $L/src/*.cc))) ; $(CC) -shared $(subst src,lib,$(patsubst %.cc,%.o,$(wildcard $L/src/*.cc))) -o $L/lib/lib$L.so))

# Compile .cc files in each library
$(foreach L,$(LIBS),$(foreach F,$(wildcard $L/src/*.cc),\
$(eval $(subst src,lib,$(patsubst %.cc,%.o,$F)): $F ; $(CC) -c -fPIC -c $(CFLAGS) $F -o  $(subst src,lib,$(patsubst %.cc,%.o,$F)))))

clean:
	rm -f */lib/*.o ./$(EXECUTABLE)

