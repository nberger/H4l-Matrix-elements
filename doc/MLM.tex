\documentclass[12pt]{article}
\usepackage{amsmath,amsthm,bm,xcolor}

\renewcommand{\familydefault}{\sfdefault}

% Redefine the page size
\RequirePackage{geometry}
\geometry{
  a4paper,%
  textwidth=16cm,% 
  textheight=23.2cm,%
  marginparsep=7pt,% 
  marginparwidth=2.5cm,
  margin=2cm%
}

\newcommand{\m}{\ensuremath{\mathcal{M}}}
\newcommand{\M}{\ensuremath{\bar{\mathcal{M}}}}
\renewcommand{\vec}{\ensuremath{\mathbf{c}}}
\newcommand{\vex}{\ensuremath{\mathbf{x}}}
\newcommand{\vev}{\ensuremath{\mathbf{v}}}
\newcommand{\val}{\ensuremath{\mathcal{V}}}
\newcommand{\samp}{\ensuremath{^{\text{MC}}}}
\newcommand{\sig}{\ensuremath{_{\text{sig}}}}
\newcommand{\bkg}{\ensuremath{_{\text{bkg}}}}
\renewcommand*{\arraystretch}{1.5}

\title{EFT sensitivity studies using the matrix element method}
\author{}
\date{}
%\date{\today}

\begin{document}
\maketitle

\section{Goal}

The STXS bins provide a parameterization of the Higgs production phase space, but something else is needed to cover decays. This is non-trivial mainly for the $H \rightarrow ZZ^*$ and $H \rightarrow WW^*$ processes, and in particular for $H \rightarrow ZZ^* \rightarrow 4\ell$ where the decay-side kinematics can be fully reconstructed from the charged leptons.

The goal of this study is to quantify the effect of BSM modifications to Higgs boson physics on the observables in this mode, and to identify bins of well-chosen quantities which could be used to encode the BSM effects. BSM physics is represented as the effect of the dimension 6 SMEFT operators, as implemented in SMEFTsim run within MG5\_aMCatNLO. Only linear SMEFT effects are considered.

\section{Inputs}

We consider the signal process $gg \rightarrow H \rightarrow ZZ^* \rightarrow 4\ell$, along with the representative background process $q\bar{q} \rightarrow ZZ^* \rightarrow 4\ell$. We obtain from MadGraph the "matrix elements" (MEs)
\begin{itemize}
\item $\m_0(\vex)$ for the SM $gg \rightarrow H \rightarrow ZZ^* \rightarrow 4\ell$
\item $\m\sig(\vex, \vec)$ for $gg \rightarrow H \rightarrow ZZ^* \rightarrow 4\ell$ in the SMEFT, with Wilson coefficient values \vec
\item $\m\bkg(\vex)$ for the background process $q\bar{q} \rightarrow ZZ^* \rightarrow 4\ell$.
\end{itemize}
where \vex\ are the event kinematic quantities : the invariant masses $m_{12}$ and $m_{34}$ of the two $Z$ candidates, the  angular variables in $1502.03045$ and the momenta of initial-state particles. These "matrix elements" actually denote differential distributions of events corresponding to a given integrated luminosity.

In the study, we set the background normalization from the SM signal normalization such that $N_0/N\bkg = 3$, where $N_0$ is the expected number of SM events, approximately matching the experimental conditions. In general we denote by $N\sig(\vec)$ the normalization of the signal with Wilson coefficient values \vec\ (so that $N_0 = N\sig(0)$).

We only consider interference contributions from the EFT in the signal ME, which is therefore a linear function of the EFT coefficients and can be written
\begin{equation}
\m\sig(\vex; \vec) = \m_0(\vex)  + \sum\limits_{a} \m'_a(\vex_i) \, c_a
\end{equation}
where $\m'_a(\vex_i)$ is the derivative of the signal ME with respect to the component of \vec\ with index $a$.


We also consider a SM signal sample $\{\vex\samp_i\}, i=1 \ldots n\samp$.

\section{Analysis}

\subsection{Expected sensitivity}

We consider the measurement of the SMEFT parameters \vec. An overall normalization factor $\nu$ is also considered as a measurement parameter, to be consistent with realistic measurements in which normalization will be measured through the STXS. This ensures that the \vec\ measurements are driven by shape information, and not spuriously by normalization effects.

To extract this information, we build the likelihood
\begin{equation}
L(\vec, \nu; \{x_i\}_{i=1 \ldots n}) = e^{-\nu N\sig(\vec) - N\bkg} \prod\limits_{i=1}^n \left[ \nu N\sig(\vec)\, p\sig(\vex_i; \vec) + N\bkg\, p\bkg(\vex_i)\right]
\end{equation}
where $p\sig$ and $p\bkg$ are the signal and background PDFs.

Given the expression above, it is economical to express the likelihood in terms of the matrix elements, $\m\sig(\vex; \vec) = N\sig(\vec)\, p\sig(\vex; \vec)$ and $\m\bkg(\vex) = N\bkg\, p\bkg(\vex)$, so that
\begin{equation}
L(\vec, \nu; \{x_i\}_{i=1 \ldots n}) = e^{-\nu N\sig(\vec) - N\bkg} \prod\limits_{i=1}^n \left[ \nu \m\sig(\vex_i; \vec) + \m\bkg(\vex_i)\right]
\end{equation}

The measurement is performed using the log-likelihood ratio
% \begin{equation}
% \lambda(\vec, \nu) = -2 \log \frac{L(\vec, \nu)}{L(\hat{\vec}, \hat{\nu})}
% = -2 \sum\limits_{i=1}^{n\samp} \log \frac{\nu N\sig(\vec) p\sig(\vex_i; \vec) + N\bkg p\bkg(\vex_i)}{\hat{\nu} N\sig(\hat{\vec}) p\sig(\vex_i; \hat{\vec}) + N\bkg p\bkg(\vex_i)} + 2 \left[\nu  N\sig(\vec) - \hat{\nu}N\sig(\hat{\vec})\right]
% \end{equation}
\begin{equation}
\lambda(\vec, \nu) = -2 \log \frac{L(\vec, \nu)}{L(\hat{\vec}, \hat{\nu})}
= -2 \sum\limits_{i=1}^{n} \log \frac{\nu \m\sig(\vex_i; \vec) + \m\bkg(\vex_i)}{\hat{\nu}\m\sig(\vex_i; \hat{\vec}) + \m\bkg(\vex_i)} + 2 \left[\nu  N\sig(\vec) - \hat{\nu}N\sig(\hat{\vec})\right]
\end{equation}
where the best-fit value of a parameter $X$ is denoted as $\hat{X}$.

We study SM Asimov sensitivities, so $\hat{\vec} = 0$ and $\hat{\nu} = 1$. The sum above should also be evaluated on a SM Asimov dataset, but what we have is the signal sample described above. We can make use of it with the appropriate reweighting, representing the Asimov sum as

\begin{align}
\sum\limits_{x_i \sim A} f(\vex_i) &\approx \int d\vex\, \left[ \m\sig(\vex;0) + \m\bkg(\vex) \right] f(\vex_i)\\
&= N_0 \int d\vex\,  p\sig(\vex; 0) \frac{\m_0(\vex) + \m\bkg(\vex)}{\m_0(\vex)} f(\vex) \\
&\approx \frac{N_0}{n\samp} \sum_{i=1}^{n\samp}\left[ \frac{ \m_0(\vex_i) + \m\bkg(\vex_i)}{\m_0(\vex)} \right] f(\vex_i)
\end{align}

(using the approximation $\sum\limits_{i=1}^n f(\vex_i) \approx n\int d\vex \,p(\vex)\, f(\vex)$ for a sample with $\vex_i \sim p$.)

We work in the Gaussian approximation, and estimate the uncertainties in the measurement using the inverse of the covariance matrix
\begin{equation}
H_{\mu_a \mu_b} = \frac{1}{2} \left. \frac{\partial^2 \lambda(\mu)}{\partial \mu_a \partial \mu_b}\right|_{c=0,\nu = 1}
\end{equation}
where $\bm{\mu} = (\vec, \nu)$.
From the expressions above, we have 
\begin{equation}
\frac{1}{2}\frac{\partial^2 \lambda(\mu)}{\partial c_a \partial c_b} = \nu^2 \sum\limits_{i=1}^{n} \frac{\m'_a(\vex_i)\m'_b(\vex_i)}{\left(\nu \m\sig(\vex_i; \vec) + \m\bkg(\vex_i)\right)^2}
\end{equation}
so that
\begin{equation}
H_{c_a c_b} = \frac{N_0}{n\samp}\sum\limits_{i=1}^{n\samp} \frac{\m'_a(\vex_i)\m'_b(\vex_i)}{\left[\m_0(\vex_i) + \m\bkg(\vex_i)\right] \m_0(\vex_i)} 
\end{equation}
for the terms not involving normalization. We have used the fact that second derivatives of the MEs vanish, since only linear effects in the $c_a$ are considered. 
%The prefactor is correcting for the fact that the number of events $n\samp$ in the sample is not necessarily equal to the required $N_0$.

We also have
\begin{equation}
\frac{1}{2}\frac{\partial^2 \lambda(\mu)}{\partial c_a \partial \nu} = - \sum\limits_{i=1}^{n} \frac{\m'_a(\vex_i)\m\bkg(\vex_i)}{\left(\nu \m\sig(\vex_i; \vec) + \m\bkg(\vex_i)\right)^2} + N'_a(\vec),
\end{equation}
where $N'_a = \partial N\sig/ \partial c_a$,
so that
\begin{equation}
H_{c_a \nu} = -\frac{N_0}{n\samp}\sum\limits_{i=1}^{n\samp} \frac{\m'_a(\vex_i)\m\bkg}{\left[\m_0(\vex_i) + \m\bkg(\vex_i)\right] \m_0(\vex_i)}  + N'_a(0).
\end{equation}
The expression for $N\sig(c)$ and its derivatives are obtained by integrating the matrix elements. We can perform the normalization integrals by summing over the signal sample defined above:
\begin{equation}
N\sig(c) = \int d\vex \, \m\sig(\vex;\vec) = \int d\vex \, \m_0(\vex)  \, \frac{\m\sig(\vex;\vec)}{\m_0(\vex)} 
\approx \frac{N_0}{n\samp}\sum\limits_{i=1}^{n\samp} \frac{\m\sig(\vex_i;\vec)}{\m_0(\vex_i)}
\end{equation}
and therefore
\begin{equation}
N'_a \approx \frac{N_0}{n\samp}\sum\limits_{i=1}^{n\samp} \frac{\m'_a(\vex_i)}{\m_0(\vex_i)}.
\end{equation}
Plugging this into the expression above leads to
\begin{equation}
H_{c_a \nu} = \frac{N_0}{n\samp}\sum\limits_{i=1}^{n\samp} \frac{\m'_a(\vex_i)}{\m_0(\vex_i) + \m\bkg(\vex_i)}.
\end{equation}

Similarly,
\begin{equation}
\frac{1}{2}\frac{\partial^2 \lambda(\mu)}{\partial \nu^2} = \sum\limits_{i=1}^{n} \frac{\m(\vex_i,\vec)^2}{\left(\nu \m\sig(\vex_i; \vec) + \m\bkg(\vex_i)\right)^2},
\end{equation}
and so
\begin{equation}
H_{\nu \nu} = \frac{N_0}{n\samp}\sum\limits_{i=1}^{n\samp} \frac{\m_0(\vex_i)}{\m_0(\vex_i) + \m\bkg(\vex_i)}.
\end{equation}

The $H$ matrix can then be diagonalized. This yields eigenvectors $\vev_{\alpha}$ with coordinates $V_{\alpha a}$ in \vec-space, as well as eigenvalues $\val_{\alpha}$. The $\vev_{\alpha}$ correspond to the directions in \vec-space which are independently measured; $\val_{\alpha}$ is the inverse square of the measurement uncertainty along this direction. It is therefore sufficient to consider only the directions with the largest eigenvalues, which are the best-measured ones.

\section{Variation weights}

As mentioned above, we only have access to a signal-only SM sample. To obtain distributions on other scenarios, we use a reweighting procedure based on the MEs. 

To include the effect of background, we need a weight corresponding to $(\m_0(\vex) + \m\bkg(\vex))/\m_0(\vex)$. However this weight would also leads to a change in the normalization of the distribution, since the numerator and denominator are normalized differently. In this case we know by definition that the numerator integral is $4/3$ of the denominator integral, so we can define the normalized weight as 

\begin{equation}
w\bkg(\vex) = \frac{3}{4} \frac{\m_0(\vex) + \m\bkg(\vex)}{\m_0(\vex)}.
\end{equation}

The effect of varying a Wilson coefficient in addition is parameterized as
\begin{align}
w(\vex;\vec) &= \frac{N_0}{N\sig(\vec) + N_0/3} \frac{\m\sig(\vex;\vec) + \m\bkg(\vex)}{\m_0(\vex)} \\
&= \frac{N_0}{\frac{4}{3} N_0 + \sum_a N'_a(\vec)c_a} \left[ \frac{\m_0(\vex) + \m\bkg(\vex)}{\m_0(\vex)} + \sum\limits_a\frac{\m'_a(\vex)}{\m_0(\vex)}c_a \right]\\
&=  \frac{w\bkg(\vex) + \frac{3}{4}\sum\limits_a\frac{\m'_a(\vex)}{\m_0(\vex)}c_a}{1 + \frac{3}{4}\sum\limits_a\frac{N'_a}{N_0}c_a}.
\end{align}
% In particular, a variation of one unit of the component of \vec\ with index $a$ can be obtained using the weight
% \begin{equation}
% w_a(\vex) = w\bkg(\vex) \frac{1 + \frac{\m'_a(\vex)}{\m_0(\vex)}}{1 + \frac{N'_{c_a}}{N_0}}.
% \end{equation}

Similarly, the variation given by the $+\vev_{\alpha}$ eigenvector of the Hessian matrix can be obtained as
\begin{equation}
w^{\text{EV}}_{\alpha}(\vex) = \frac{w\bkg(\vex) + \frac{3}{4}\sum\limits_a\frac{\m'_a(\vex)}{\m_0(\vex)} V_{\alpha a} }{1 + \frac{3}{4}\sum\limits_a\frac{N'_a}{N_0} V_{\alpha a} }.
\end{equation}

\end{document}
